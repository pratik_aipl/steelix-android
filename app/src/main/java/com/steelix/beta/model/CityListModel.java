package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class CityListModel implements Serializable {


    @JsonField
    int citiesid;
    @JsonField
    String citiesname;

    public int getCitiesid() {
        return citiesid;
    }

    public void setCitiesid(int citiesid) {
        this.citiesid = citiesid;
    }

    public String getCitiesname() {
        return citiesname;
    }

    public void setCitiesname(String citiesname) {
        this.citiesname = citiesname;
    }
}
