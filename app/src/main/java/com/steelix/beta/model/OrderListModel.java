package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class OrderListModel implements Serializable {

    @JsonField
    int OrderID;
    @JsonField
    int qty;
    @JsonField
    String DistributorName;
    @JsonField
    String OrderDate;
    @JsonField
    String OrderRefNo;
    @JsonField
    String DealerID;
    @JsonField
    String DistributorID;
    @JsonField
    String StatusCode;
    @JsonField
    String StatusColorCode;
    @JsonField
    boolean isHeader;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDistributorName() {
        return DistributorName;
    }

    public void setDistributorName(String distributorName) {
        DistributorName = distributorName;
    }

    public String getStatusColorCode() {
        return StatusColorCode;
    }

    public void setStatusColorCode(String statusColorCode) {
        StatusColorCode = statusColorCode;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderRefNo() {
        return OrderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        OrderRefNo = orderRefNo;
    }

    public String getDealerID() {
        return DealerID;
    }

    public void setDealerID(String dealerID) {
        DealerID = dealerID;
    }

    public String getDistributorID() {
        return DistributorID;
    }

    public void setDistributorID(String distributorID) {
        DistributorID = distributorID;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }
}
