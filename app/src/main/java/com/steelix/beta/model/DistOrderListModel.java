package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class DistOrderListModel implements Serializable {

    @JsonField
    int OrderID;
    @JsonField
    int DealerID;
    @JsonField
    int totalQty;
    @JsonField
    String FirstName;
    @JsonField
    String LastName;
    @JsonField
    int StatusID;
    @JsonField
    String StatusCode;
    @JsonField
    String StatusColorCode;
    @JsonField
    String OrderDate;
    @JsonField
    boolean isHeader;
    int modelPos =-1;

    public String getStatusColorCode() {
        return StatusColorCode;
    }

    public void setStatusColorCode(String statusColorCode) {
        StatusColorCode = statusColorCode;
    }

    public int getModelPos() {
        return modelPos;
    }

    public void setModelPos(int modelPos) {
        this.modelPos = modelPos;
    }

    public int getDealerID() {
        return DealerID;
    }

    public void setDealerID(int dealerID) {
        DealerID = dealerID;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getStatusID() {
        return StatusID;
    }

    public void setStatusID(int statusID) {
        StatusID = statusID;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }
}
