package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class DisributorListModel implements Serializable {

    @JsonField
    int DistributorID;
    @JsonField
    String CompanyName;
    @JsonField
    String MobileNo;
    @JsonField
    String EmailID;


    public DisributorListModel() {
    }

    public int getDistributorID() {
        return DistributorID;
    }

    public void setDistributorID(int distributorID) {
        DistributorID = distributorID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }
}
