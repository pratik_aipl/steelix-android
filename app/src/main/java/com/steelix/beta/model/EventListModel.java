package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class EventListModel implements Serializable {

    @JsonField
    String EventTitle;
    @JsonField
    String EventDescription;
    @JsonField
    List<ProductsImage>event_gallary;


    public String getEventTitle() {
        return EventTitle;
    }

    public void setEventTitle(String eventTitle) {
        EventTitle = eventTitle;
    }

    public String getEventDescription() {
        return EventDescription;
    }

    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }

    public List<ProductsImage> getEvent_gallary() {
        return event_gallary;
    }

    public void setEvent_gallary(List<ProductsImage> event_gallary) {
        this.event_gallary = event_gallary;
    }
}
