package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
@JsonObject
public class ImageListModel implements Serializable {


    @JsonField(name = "id")
    int idD;
    @JsonField
    String ProductID;
    @JsonField
    String image;

    public int getIdD() {
        return idD;
    }

    public void setIdD(int id) {
        this.idD = id;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
