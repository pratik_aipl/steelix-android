package com.steelix.beta.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class StatusListModel implements Serializable {


    @JsonField
    int StatusID;
    @JsonField
    String StatusCode;
    @JsonField
    String StatusColorCode;

    public String getStatusColorCode() {
        return StatusColorCode;
    }

    public void setStatusColorCode(String statusColorCode) {
        StatusColorCode = statusColorCode;
    }

    public int getStatusID() {
        return StatusID;
    }

    public void setStatusID(int statusID) {
        StatusID = statusID;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }
}
