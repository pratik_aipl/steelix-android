package com.steelix.beta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class CompanyOfferModel implements Serializable {

    @JsonField
    int OfferID;
    @JsonField
    String offerType;
    @JsonField
    String offerImageUrl;
    @JsonField
    String offerStartDate;
    @JsonField
    String OfferEndDate;

    public int getOfferID() {
        return OfferID;
    }

    public void setOfferID(int offerID) {
        OfferID = offerID;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferImageUrl() {
        return offerImageUrl;
    }

    public void setOfferImageUrl(String offerImageUrl) {
        this.offerImageUrl = offerImageUrl;
    }

    public String getOfferStartDate() {
        return offerStartDate;
    }

    public void setOfferStartDate(String offerStartDate) {
        this.offerStartDate = offerStartDate;
    }

    public String getOfferEndDate() {
        return OfferEndDate;
    }

    public void setOfferEndDate(String offerEndDate) {
        OfferEndDate = offerEndDate;
    }
}
