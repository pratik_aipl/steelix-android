package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.steelix.beta.R;
import com.steelix.beta.model.EventListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventPicListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<EventListModel> eventListModels;

    // private ArrayList<Integer> IMAGES;

    public EventPicListAdapter(Context context, List<EventListModel> eventListModels) {
        this.context = context;
        this.eventListModels = eventListModels;
    }

   /* public EventPicListAdapter(Context context, ArrayList<Integer> IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_pic_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        EventListModel eventListModel = eventListModels.get(position);
        //Integer[] IMAGES = {R.drawable.coockerp, R.drawable.logo, R.drawable.coockerp};

        holder.pager.setAdapter(new PagerSliderAdapter(context, eventListModel.getEvent_gallary()));
        holder.indicator.setupWithViewPager(holder.pager);
        holder.tvEventname.setText(eventListModel.getEventTitle());
        holder.tvEdesc.setText(Html.fromHtml(eventListModel.getEventDescription()));

    }

    @Override
    public int getItemCount() {
        return eventListModels.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pager)
        ViewPager pager;
        //WormDotsIndicator indicator;
        ViewPagerIndicator indicator;
        @BindView(R.id.tv_Eventname)
        TextView tvEventname;
        @BindView(R.id.tv_Edesc)
        TextView tvEdesc;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            indicator = view.findViewById(R.id.indicator);
        }
    }

}