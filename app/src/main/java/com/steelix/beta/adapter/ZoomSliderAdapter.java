package com.steelix.beta.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.steelix.beta.R;
import com.steelix.beta.model.CompanyOfferModel;
import com.steelix.beta.model.ProductsImage;
import com.steelix.beta.utils.L;

import java.util.List;

public class ZoomSliderAdapter extends PagerAdapter {


    private List<ProductsImage> eventListModels;
    private List<CompanyOfferModel> companyOfferModels;
    private LayoutInflater inflater;
    private Context context;
    boolean isOffer = false;

    public ZoomSliderAdapter(Context context, List<ProductsImage> eventListModels) {
        this.context = context;
        this.eventListModels = eventListModels;
        this.isOffer = false;
        inflater = LayoutInflater.from(context);
    }

    public ZoomSliderAdapter(Context context, List<CompanyOfferModel> companyOfferModel, boolean isOffer) {
        this.context = context;
        this.companyOfferModels = companyOfferModel;
        inflater = LayoutInflater.from(context);
        this.isOffer = isOffer;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {

        return isOffer ? companyOfferModels.size() : eventListModels.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.zoom_pager_item, view, false);

        PhotoView photoView = imageLayout.findViewById(R.id.img_zoom);
        assert imageLayout != null;
        if (isOffer) {
            CompanyOfferModel companyOfferModel = companyOfferModels.get(position);
            L.loadImageWithPicasso(context, companyOfferModel.getOfferImageUrl(), photoView, null);
        } else {
            ProductsImage eventListModel = eventListModels.get(position);
            L.loadImageWithPicasso(context, eventListModel.getImageURL(), photoView, null);
        }
        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}