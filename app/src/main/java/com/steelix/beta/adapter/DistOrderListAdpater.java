package com.steelix.beta.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.activity.OrderDetailsActivity;
import com.steelix.beta.model.DistOrderListModel;
import com.steelix.beta.utils.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DistOrderListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<DistOrderListModel> orderListModels;
    LayoutInflater inflater;

    public DistOrderListAdpater(Context context, List<DistOrderListModel> listItemArrayList) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.orderListModels = listItemArrayList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolderChild(inflater.inflate(R.layout.orderlist_row_item_design, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {
        DistOrderListModel orderListModel = orderListModels.get(position);
        MyViewHolderChild vaultItemHolder = (MyViewHolderChild) holderIn;
        vaultItemHolder.tvOrderno.setText("" + orderListModel.getOrderID());
        vaultItemHolder.tvStatus.setText(orderListModel.getStatusCode());
        vaultItemHolder.tvStatus.setBackground(L.changeDrawableColor(context, R.drawable.button, Color.parseColor(orderListModel.getStatusColorCode())));
        vaultItemHolder.tvName.setText(orderListModel.getFirstName() + " " + orderListModel.getLastName());
        vaultItemHolder.tvQty.setText("" + orderListModel.getTotalQty());
        if (orderListModel.isHeader()) {
            vaultItemHolder.mHeaderDate.setVisibility(View.VISIBLE);
            vaultItemHolder.tvDate.setText(L.getDate(orderListModel.getOrderDate()));
        } else {
            vaultItemHolder.mHeaderDate.setVisibility(View.GONE);
        }

        vaultItemHolder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, OrderDetailsActivity.class)
                    .putExtra("OrderID", orderListModel.getOrderID())
                    .putExtra("OrderDate", orderListModel.getOrderDate())
                    .putExtra("StatusColour", orderListModel.getStatusColorCode())
                    .putExtra("Status", orderListModel.getStatusCode())
                    .putExtra("OrderNo", orderListModel.getOrderID()));

        });
    }

    @Override
    public int getItemCount() {
        return orderListModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    class MyViewHolderChild extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.mHeaderDate)
        RelativeLayout mHeaderDate;
        @BindView(R.id.tv_orderno)
        TextView tvOrderno;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_Qty)
        TextView tvQty;

        public MyViewHolderChild(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}