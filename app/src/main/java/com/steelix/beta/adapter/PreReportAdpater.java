package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.ReportPreListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreReportAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ReportPreListModel> dashmodels;


    public PreReportAdpater(Context context, List<ReportPreListModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pre_report_row_iteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ReportPreListModel dashmodel = dashmodels.get(position);

        holder.tvReportTittle.setText(dashmodel.getName());
        holder.tvSalesCount.setText("" + dashmodel.getTotal());
        holder.tvReportDate.setText(dashmodel.getDate());


    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_report_tittle)
        TextView tvReportTittle;
        @BindView(R.id.tv_sales_count)
        TextView tvSalesCount;
        @BindView(R.id.tv_Report_date)
        TextView tvReportDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}