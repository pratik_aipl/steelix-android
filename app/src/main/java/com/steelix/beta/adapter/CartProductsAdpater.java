package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.activity.CartActivity;
import com.steelix.beta.listner.RemoveProduct;
import com.steelix.beta.model.ProductsListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartProductsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "CartProductsAdpater";
    Context context;
    List<ProductsListModel> dashmodels;
    RemoveProduct removeProduct;

    public CartProductsAdpater(CartActivity context, List<ProductsListModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
        removeProduct = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_iteam_design, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ProductsListModel dashmodel = dashmodels.get(position);

          /* if (productsListModels.get(position - 1).getProductName() != null) {
                        L.loadImageWithPicasso(NewOrderActivity.this, productsListModels.get(position - 1).getProductName(), imgProduct, null);
                    }*/

        Log.d(TAG, "onBindViewHolder: "+ dashmodel.getCartQty());
        holder.tvPname.setText(dashmodel.getProductName());
        holder.tvPcapacity.setText(dashmodel.getCapacity());
        holder.tvPqty.setText("" + dashmodel.getCartQty());
        holder.dltprodct.setOnClickListener(view -> {
            removeProduct.onRemoveProduct(dashmodel,position);
        });
    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_product)
        ImageView imgProduct;
        @BindView(R.id.btn_remove)
        Button dltprodct;
        @BindView(R.id.tv_Pname)
        TextView tvPname;
        @BindView(R.id.tv_Pcapacity)
        TextView tvPcapacity;
        @BindView(R.id.tv_Pqty)
        TextView tvPqty;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}