package com.steelix.beta.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;
import com.steelix.beta.R;
import com.steelix.beta.model.CompanyOfferModel;
import com.steelix.beta.utils.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompanyOfferAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CompanyOfferModel> dashmodels;

    public CompanyOfferAdpater(Context context, List<CompanyOfferModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.company_offer_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CompanyOfferModel dashmodel = dashmodels.get(position);

        if (dashmodel.getOfferImageUrl() != null) {
            L.loadImageWithPicasso(context, dashmodel.getOfferImageUrl(), holder.imgOffer, null);
        }

        holder.itemView.setOnClickListener(view1 -> {
            Dialog dialog;
            dialog = new Dialog(context, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.zoom_pager_item);
            PhotoView photoView = dialog.findViewById(R.id.img_zoom);
            if (dashmodel.getOfferImageUrl() != null) {
                L.loadImageWithPicasso(context, dashmodel.getOfferImageUrl(), photoView, null);
            }
            dialog.show();
        });
    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_offer)
        ImageView imgOffer;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}