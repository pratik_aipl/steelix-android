package com.steelix.beta.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.steelix.beta.R;
import com.steelix.beta.activity.ZoomImagePager;
import com.steelix.beta.model.ProductsImage;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import java.io.Serializable;
import java.util.List;

public class PagerSliderAdapter extends PagerAdapter {


    private List<ProductsImage> eventListModels;
    private LayoutInflater inflater;
    private Context context;
    // Integer[] IMAGES;

    public PagerSliderAdapter(Context context, List<ProductsImage> eventListModels) {
        this.context = context;
        this.eventListModels = eventListModels;
        inflater = LayoutInflater.from(context);
    }
  /*  public PagerSliderAdapter(Context context, Integer[] IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);
    }*/

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return eventListModels.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.pager_item, view, false);

        assert imageLayout != null;
        ProductsImage eventListModel = eventListModels.get(position);

        final ImageView imageView = imageLayout.findViewById(R.id.img_pager);

        L.loadImageWithPicasso(context, eventListModel.getImageURL(), imageView, null);

        view.addView(imageLayout, 0);

        imageView.setOnClickListener(view1 -> {
            Intent myIntent = new Intent(context, ZoomImagePager.class)
                    .putExtra("SFrom", "event")
                    .putExtra(Constant.PAGERIMAGE, (Serializable) eventListModels);
            context.startActivity(myIntent);


        });


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}