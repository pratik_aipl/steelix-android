package com.steelix.beta.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.listner.ChangeStatus;
import com.steelix.beta.model.DistOrderListModel;
import com.steelix.beta.model.StatusListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.network.RestAPIBuilder;
import com.steelix.beta.network.RestApi;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class EventListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ChangeStatus {

    private static final String TAG = "EventListAdapter";
    Context context;
    List<DistOrderListModel> dashmodels;
    List<StatusListModel> statusListModels;
    Dialog profiledialog;
    Subscription subscription;
    protected RestApi restApi;
    ProgressDialog progressDialog;

    public EventListAdapter(Context context, List<DistOrderListModel> dashmodels, List<StatusListModel> statusListModels) {
        this.context = context;
        this.dashmodels = dashmodels;
        this.statusListModels = statusListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder vaultItemHolder = (ViewHolder) holderIn;
        DistOrderListModel orderListModel = dashmodels.get(position);
        restApi = RestAPIBuilder.buildRetrofitService();


        vaultItemHolder.tvOrderno.setText("" + orderListModel.getOrderID());
        vaultItemHolder.tvStatus.setText(orderListModel.getStatusCode());
        vaultItemHolder.tvStatus.setBackground(L.changeDrawableColor(context, R.drawable.button, Color.parseColor(orderListModel.getStatusColorCode())));

        vaultItemHolder.tvName.setText(orderListModel.getFirstName() + " " + orderListModel.getLastName());

        if (orderListModel.isHeader()) {
            vaultItemHolder.mHeaderDate.setVisibility(View.VISIBLE);
            vaultItemHolder.tvDate.setText(L.getDate(orderListModel.getOrderDate()));
        } else {
            vaultItemHolder.mHeaderDate.setVisibility(View.GONE);
        }

        if (orderListModel.getModelPos() != -1) {
            vaultItemHolder.tvStatus.setText(statusListModels.get(orderListModel.getModelPos()).getStatusCode());
            vaultItemHolder.tvStatus.setBackground(L.changeDrawableColor(context, R.drawable.button, Color.parseColor(orderListModel.getStatusColorCode())));
        }

        vaultItemHolder.tvStatus.setOnClickListener(view -> {
            profiledialog = new Dialog(context, R.style.Theme_Dialog);
            profiledialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            profiledialog.setCancelable(true);
            profiledialog.setContentView(R.layout.status_dialog);

            RecyclerView recyclerlist;
            recyclerlist = profiledialog.findViewById(R.id.recycler_list);
            recyclerlist.setHasFixedSize(true);
            recyclerlist.setItemAnimator(new DefaultItemAnimator());
            StatusListAdpater SubEventAdpater;
            SubEventAdpater = new StatusListAdpater(context, this, statusListModels, orderListModel.getStatusID(), position);
            recyclerlist.setAdapter(SubEventAdpater);
            profiledialog.show();

        });
    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }

    @Override
    public void onChangePStatus(int pos, int modelId, String status, int selectedListItemPos) {
        profiledialog.dismiss();
        this.dashmodels.get(selectedListItemPos).setModelPos(pos);
        this.dashmodels.get(selectedListItemPos).setStatusID(statusListModels.get(pos).getStatusID());
        this.dashmodels.get(selectedListItemPos).setStatusID(statusListModels.get(pos).getStatusID());
        this.dashmodels.get(selectedListItemPos).setStatusCode(statusListModels.get(pos).getStatusCode());
        this.dashmodels.get(selectedListItemPos).setStatusColorCode(statusListModels.get(pos).getStatusColorCode());
        notifyDataSetChanged();
        ChangeOrderStatus(modelId, selectedListItemPos);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.mHeaderDate)
        LinearLayout mHeaderDate;
        @BindView(R.id.tv_orderno)
        TextView tvOrderno;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_status)
        TextView tvStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void ChangeOrderStatus(int modelId, int pos) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.OrderID, String.valueOf(dashmodels.get(pos).getOrderID()));
        map.put(Constant.StatusID, String.valueOf(modelId));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.ChangeOrderStatus(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {

            } else {
                L.serviceStatusFalseProcess(context, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}