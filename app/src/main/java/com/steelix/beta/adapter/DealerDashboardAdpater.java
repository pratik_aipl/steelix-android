package com.steelix.beta.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.DisributorListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DealerDashboardAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<DisributorListModel> dashmodels;

    public DealerDashboardAdpater(Context context, List<DisributorListModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dealer_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        final DisributorListModel dashmodel = dashmodels.get(position);

        holder.tv_cmpnyName.setText(dashmodel.getCompanyName());
        holder.tv_mobileNo.setText(dashmodel.getMobileNo());
        holder.tv_emailId.setText(dashmodel.getEmailID());
        holder.tv_mobileNo.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL,
                    Uri.fromParts("tel", dashmodel.getMobileNo(), null));
            context.startActivity(intent);
        });
        holder.tv_emailId.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + dashmodel.getEmailID()));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Demo");
            intent.putExtra(Intent.EXTRA_TEXT, "Hii");
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_cmpnyName)
        TextView tv_cmpnyName;
        @BindView(R.id.tv_mobileNo)
        TextView tv_mobileNo;
        @BindView(R.id.tv_emailId)
        TextView tv_emailId;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}