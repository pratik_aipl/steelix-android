package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.steelix.beta.R;
import com.steelix.beta.listner.ChangeStatus;
import com.steelix.beta.model.StatusListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<StatusListModel> dashmodels;
    ChangeStatus changeStatus;
    int StatusID,selectedListItemPos;

    public StatusListAdpater(Context context, EventListAdapter eventListAdapter, List<StatusListModel> dashmodels, int statusID, int selectedListItemPos) {
        this.context = context;
        this.dashmodels = dashmodels;
        this.changeStatus = eventListAdapter;
        this.StatusID = statusID;
        this.selectedListItemPos = selectedListItemPos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.status_list_row_iteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn,int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        StatusListModel dashmodel = dashmodels.get(position);

//        Log.d(TAG, "onBindViewHolder: ");
        holder.rdStatus.setText(dashmodel.getStatusCode());
        for (int i = 0; i < dashmodels.size(); i++) {
            if (dashmodel.getStatusID() == StatusID) {
                holder.rdStatus.setChecked(true);
                break;
            }else
                holder.rdStatus.setChecked(false);
        }
        holder.rdStatus.setOnClickListener(view -> {
            dashmodel.getStatusID();
          //  Toast.makeText(context, "" + dashmodel.getStatusID(), Toast.LENGTH_SHORT).show();
            changeStatus.onChangePStatus(position,dashmodel.getStatusID(), dashmodel.getStatusCode(),selectedListItemPos);
        });

    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.rd_status)
        RadioButton rdStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}