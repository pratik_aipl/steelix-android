package com.steelix.beta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.ProductsListModel;

import java.util.List;


public class GetProductListAdapter extends BaseAdapter {
    List<ProductsListModel> productsListModel;
    LayoutInflater inflter;
    Context context;

    public GetProductListAdapter(Context context, List<ProductsListModel> productsListModel) {
        this.context = context;
        this.productsListModel = productsListModel;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return productsListModel.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Products");
            names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));
        } else {
            ProductsListModel productsModel = productsListModel.get(position - 1);
            names.setText(productsModel.getProductName());
            //  names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}