package com.steelix.beta.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.activity.OrderDetailsActivity;
import com.steelix.beta.model.OrderListModel;
import com.steelix.beta.utils.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<OrderListModel> orderListModels;

    //  private static final int LAYOUT_HEADER = 0;
//    private static final int LAYOUT_CHILD = 1;

    private LayoutInflater inflater;

    public OrderListAdpater(Context context, List<OrderListModel> listItemArrayList) {

        inflater = LayoutInflater.from(context);
        this.context = context;
        this.orderListModels = listItemArrayList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;

        View view = inflater.inflate(R.layout.orderlist_row_item_design, parent, false);
        holder = new MyViewHolderChild(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {
        // ViewHolder holder = (CompanyOfferAdpater.ViewHolder) holderIn;
        OrderListModel orderListModel = orderListModels.get(position);

  /*      if (holderIn.getItemViewType() == LAYOUT_HEADER) {
            MyViewHolderHeader vaultItemHolder = (MyViewHolderHeader) holderIn;
            vaultItemHolder.tvHeader.setText(L.getDate(orderListModel.getOrderDate()));
        } else {*/

        MyViewHolderChild vaultItemHolder = (MyViewHolderChild) holderIn;
        vaultItemHolder.tvOrderno.setText("" + orderListModel.getOrderRefNo());
        vaultItemHolder.tvStatus.setText(orderListModel.getStatusCode());
        vaultItemHolder.tvQty.setText("" + orderListModel.getQty());
        vaultItemHolder.tvName.setText(orderListModel.getDistributorName());
        vaultItemHolder.tvStatus.setBackground(L.changeDrawableColor(context, R.drawable.button, Color.parseColor(orderListModel.getStatusColorCode())));

        if (orderListModel.isHeader()) {
            vaultItemHolder.mHeaderDate.setVisibility(View.VISIBLE);
            vaultItemHolder.tvDate.setText(L.getDate(orderListModel.getOrderDate()));
        } else {
            vaultItemHolder.mHeaderDate.setVisibility(View.GONE);
        }
        vaultItemHolder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, OrderDetailsActivity.class)
                    .putExtra("OrderID", orderListModel.getOrderID())
                    .putExtra("OrderDate", orderListModel.getOrderDate())
                    .putExtra("StatusColour", orderListModel.getStatusColorCode())
                    .putExtra("Status", orderListModel.getStatusCode())
                    .putExtra("OrderNo", orderListModel.getOrderRefNo()));

        });

        //  }

    }

    @Override
    public int getItemCount() {
        return orderListModels.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (orderListModels.get(position).isHeader()) {
//            return LAYOUT_HEADER;
//        } else
        return position;
    }

    class MyViewHolderHeader extends RecyclerView.ViewHolder {

        TextView tvHeader;

        public MyViewHolderHeader(View itemView) {
            super(itemView);
            tvHeader = itemView.findViewById(R.id.tv_date);
        }

    }

    class MyViewHolderChild extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.mHeaderDate)
        RelativeLayout mHeaderDate;
        @BindView(R.id.tv_orderno)
        TextView tvOrderno;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_Qty)
        TextView tvQty;

        public MyViewHolderChild(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}