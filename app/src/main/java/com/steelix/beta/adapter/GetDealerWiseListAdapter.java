package com.steelix.beta.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.DistOrderListModel;

import java.util.List;


public class GetDealerWiseListAdapter extends BaseAdapter {
    List<DistOrderListModel> dealerlist;
    LayoutInflater inflter;
    Context context;

    public GetDealerWiseListAdapter(Context context, List<DistOrderListModel> dealerlists) {
        this.context = context;
        this.dealerlist = dealerlists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return dealerlist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Dealer");
            names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));
        } else {
            DistOrderListModel dealerList = dealerlist.get(position - 1);
            names.setText(dealerList.getFirstName() + " " + dealerList.getLastName());
            //  names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}