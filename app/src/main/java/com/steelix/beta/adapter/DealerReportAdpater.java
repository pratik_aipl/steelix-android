package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.DealerReportListModel;
import com.steelix.beta.utils.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DealerReportAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<DealerReportListModel> dashmodels;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public DealerReportAdpater(Context context, List<DealerReportListModel> dashmodel) {
        this.context = context;
        this.dashmodels = dashmodel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new RowIteam(LayoutInflater.from(parent.getContext()).inflate(R.layout.dealer_report_list, parent, false));
        } else
            return new Header(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row_header, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {
        if (holderIn instanceof RowIteam) {
            RowIteam holder = (RowIteam) holderIn;
            DealerReportListModel dashmodel = dashmodels.get(position - 1);

            holder.tvOrderno.setText("" + dashmodel.getOrderID());
            holder.tvQty.setText("" + dashmodel.getTotalQty() + " pcs");
            holder.tvName.setText(dashmodel.getFirstName() + " " + dashmodel.getLastName());
            holder.tvDate.setText("Dt. " + L.getDate(dashmodel.getOrderDate()));
        }

    }

    @Override
    public int getItemCount() {
        return dashmodels.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    static class Header extends RecyclerView.ViewHolder {

        public Header(View itemView) {
            super(itemView);
        }
    }


    static class RowIteam extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_orderno)
        TextView tvOrderno;
        @BindView(R.id.tv_Qty)
        TextView tvQty;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_date)
        TextView tvDate;


        public RowIteam(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}