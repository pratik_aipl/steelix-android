package com.steelix.beta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.OrderDetailListModel;
import com.steelix.beta.utils.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderDetailsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<OrderDetailListModel> dashmodels;

    public OrderDetailsAdpater(Context context, List<OrderDetailListModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_detail_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        OrderDetailListModel dashmodel = dashmodels.get(position);

        if (dashmodel.getImage() != null) {
            L.loadImageWithPicasso(context, dashmodel.getImage(), holder.imgProduct, null);
        }
        holder.tvProdName.setText(dashmodel.getProductName());
        holder.tvQty.setText("" + dashmodel.getQty());

    }

    @Override
    public int getItemCount() {

        return dashmodels.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_product)
        CircleImageView imgProduct;
        @BindView(R.id.tv_prod_name)
        TextView tvProdName;
        @BindView(R.id.tv_qty)
        TextView tvQty;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}