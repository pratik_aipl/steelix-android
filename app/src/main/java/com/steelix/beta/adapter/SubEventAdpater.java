package com.steelix.beta.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.model.DistOrderListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubEventAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<DistOrderListModel> dashmodels;

    public SubEventAdpater(Context context, List<DistOrderListModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_sub_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        final DistOrderListModel dashmodel = dashmodels.get(position);

      /*  holder.tv_orderno.setText(dashmodel.getOrderNo());
        holder.tv_name.setText(dashmodel.getName());
        holder.tv_status.setText(dashmodel.getStatus());
*/
        holder.tv_status.setOnClickListener(view -> {
            Dialog profiledialog;
            profiledialog = new Dialog(context, R.style.Theme_Dialog);
            profiledialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            profiledialog.setCancelable(true);
            profiledialog.setContentView(R.layout.status_dialog);

           /* RadioGroup order_Status;
            order_Status = profiledialog.findViewById(R.id.order_Status);
            order_Status.setOnCheckedChangeListener((radioGroup, i) -> {
                switch (i) {
                    case R.id.rd_Pending:
                        profiledialog.dismiss();
                        holder.tv_status.setText("Pending");
                        break;
                    case R.id.rd_cancel:
                        Toast.makeText(context, "Canceled", Toast.LENGTH_SHORT).show();
                        profiledialog.dismiss();
                        holder.tv_status.setText("Canceled");
                        break;
                    case R.id.rd_Deliver:
                        profiledialog.dismiss();
                        holder.tv_status.setText("Delivered");
                        break;
                }
            });*/


            RecyclerView recyclerlist;
            recyclerlist = profiledialog.findViewById(R.id.recycler_list);

            recyclerlist.setHasFixedSize(true);
            recyclerlist.setItemAnimator(new DefaultItemAnimator());

//            eventListAdapter = new EventListAdapter(getActivity(), distOrderListModels);
//            recyclerlist.setAdapter(eventListAdapter);

            profiledialog.show();
        });

    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_orderno)
        TextView tv_orderno;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_status)
        TextView tv_status;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}