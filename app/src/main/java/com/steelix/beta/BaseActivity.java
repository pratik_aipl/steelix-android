package com.steelix.beta;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.model.UserData;
import com.steelix.beta.network.RestAPIBuilder;
import com.steelix.beta.network.RestApi;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.Prefs;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.lang.reflect.Type;
import java.util.List;


public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    public Type type;

    ProgressDialog progressDialog;
    public RxPermissions rxPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        type = new TypeToken<List<ProductsListModel>>() {
        }.getType();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
        progressDialog.setCancelable(false);
    }
}
