package com.steelix.beta.fregment;


import android.Manifest;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseFragment;
import com.steelix.beta.R;
import com.steelix.beta.adapter.DealerReportAdpater;
import com.steelix.beta.adapter.GetDealerWiseListAdapter;
import com.steelix.beta.adapter.GetProductListAdapter;
import com.steelix.beta.model.DealerReportListModel;
import com.steelix.beta.model.DistOrderListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.GeneratePdfClass;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.steelix.beta.utils.GeneratePdfClass.getRecyclerViewScreenshot;

public class DealerwiseReportFragment extends BaseFragment {

    private static final String TAG = "DealerwiseReportFragmen";
    List<DealerReportListModel> dealerWiseReportListModels = new ArrayList<>();

    DealerReportAdpater dealerwiseReportAdpater;
    List<ProductsListModel> productsListModels = new ArrayList<>();
    GetProductListAdapter getProductListAdapter;
    GetDealerWiseListAdapter getDealerWiseListAdapter;
    List<DistOrderListModel> DealerListModels = new ArrayList<>();

    Subscription subscription;
    LinearLayout notfound;
    @BindView(R.id.tv_fromDate)
    TextView tvFromDate;
    @BindView(R.id.tv_toDate)
    TextView tvToDate;
    @BindView(R.id.spn_Prodtlist)
    Spinner spnProdtlist;
    @BindView(R.id.li_distlist)
    LinearLayout liDistlist;
    @BindView(R.id.tv_notavl)
    TextView tvNotavl;
    DatePickerDialog datePickerDialog;

    String ToDATE = "", FromDATE = "", ProductId = "", DealerID = "";

    final Calendar myCalendar = Calendar.getInstance();
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    boolean isDeler;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    public static Fragment newInstance(boolean isDeler) {
        DealerwiseReportFragment dealerwiseReportFragment = new DealerwiseReportFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constant.page, isDeler);
        dealerwiseReportFragment.setArguments(bundle);
        return dealerwiseReportFragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutView = inflater.inflate(R.layout.activity_dist_fregment_report, container, false);
        ButterKnife.bind(this, layoutView);

        isDeler = getArguments().getBoolean(Constant.page);

        dealerwiseReportAdpater = new DealerReportAdpater(getActivity(), dealerWiseReportListModels);
        recyclerList.setAdapter(dealerwiseReportAdpater);


        if (L.isNetworkAvailable(getActivity())) {
            getDealerwiseReport(isDeler);
            if (!isDeler)
                getProductList();
            else
                getDealerList();
        }

        recyclerList.setHasFixedSize(true);
        recyclerList.setItemAnimator(new DefaultItemAnimator());


        fabAdd.setOnClickListener(view -> {
            rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribe(granted -> {
                        if (granted) {
                            GeneratePdfClass.createPdf(getRecyclerViewScreenshot(recyclerList), getActivity());
                        } else {
                            // At least one permission is denied
                        }
                    });
        });
        spnProdtlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    if (!isDeler)
                        Toast.makeText(getContext(), "Please Select Product", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), "Please Select Dealer", Toast.LENGTH_SHORT).show();
                } else {
                    if (ToDATE.compareTo(FromDATE) < 0) {
                        Toast.makeText(getContext(), "Select FromDate less then to ToDate", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!isDeler) {
                            ProductId = String.valueOf(productsListModels.get(position - 1).getProductID());
                        } else {
                            DealerID = String.valueOf(DealerListModels.get(position - 1).getDealerID());
                        }
                        getDealerwiseReport(isDeler);
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });
        return layoutView;
    }

    private void toDate() {
        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvToDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        ToDATE = sdf.format(myCalendar.getTime());

    }

    private void fromDate() {
        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvFromDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        FromDATE = sdf.format(myCalendar.getTime());


    }

    DatePickerDialog.OnDateSetListener fromdate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        fromDate();
    };
    DatePickerDialog.OnDateSetListener todate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        toDate();
    };


    private void getDealerwiseReport(boolean isDeler) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DistributorID, user.getId());
        map.put(Constant.FROM, FromDATE);
        map.put(Constant.TO, ToDATE);

        showProgress(true);
        if (isDeler) {
            map.put(Constant.DealerID, DealerID);
            subscription = NetworkRequest.performAsyncRequest(restApi.getDealerWiseReport(map), (data) -> {
                showProgress(false);
                if (data.code() == 200) {
                    try {
                        JSONObject jsonResponse = new JSONObject(data.body());
                        dealerWiseReportListModels.clear();
                        dealerWiseReportListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.REPORTLIST).toString(), DealerReportListModel.class));
                        dealerwiseReportAdpater.notifyDataSetChanged();
                        Log.d(TAG, "sizze2 " + dealerWiseReportListModels.size());

                        if (dealerWiseReportListModels.isEmpty()) {
                            fabAdd.hide();
                        }

                        errorView();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    L.serviceStatusFalseProcess(getContext(), data);
                }

            }, (e) -> {
                showProgress(false);
                e.printStackTrace();
            });
        } else {
            map.put(Constant.ProductID, ProductId);
            subscription = NetworkRequest.performAsyncRequest(restApi.getProductWiseReport(map), (data) -> {
                showProgress(false);
                if (data.code() == 200) {
                    try {
                        JSONObject jsonResponse = new JSONObject(data.body());
                        dealerWiseReportListModels.clear();
                        dealerWiseReportListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.REPORTLIST).toString(), DealerReportListModel.class));
                        dealerwiseReportAdpater.notifyDataSetChanged();
                        errorView();
                        if (dealerWiseReportListModels.isEmpty()) {
                            fabAdd.hide();
                        } else {
                            fabAdd.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    L.serviceStatusFalseProcess(getContext(), data);
                }

            }, (e) -> {
                showProgress(false);
                e.printStackTrace();
            });
        }


    }

    private void errorView() {
        if (dealerWiseReportListModels.size() == 0) {
            tvNotavl.setVisibility(View.VISIBLE);
            recyclerList.setVisibility(View.GONE);
        } else {
            tvNotavl.setVisibility(View.GONE);
            recyclerList.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.tv_fromDate, R.id.tv_toDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_fromDate:
                datePickerDialog = new DatePickerDialog(getContext(), fromdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;
            case R.id.tv_toDate:
                datePickerDialog = new DatePickerDialog(getContext(), todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }
    }

    private void getProductList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getProduct(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    productsListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.PRODUCTLIST).toString(), ProductsListModel.class));

                    getProductListAdapter = new GetProductListAdapter(getContext(), productsListModels);
                    spnProdtlist.setAdapter(getProductListAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getContext(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getDealerList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DistributorID, user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getAssginDealerList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    DealerListModels.clear();
                    if (jsonResponse.has(Constant.DEALERLIST))
                        DealerListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DEALERLIST).toString(), DistOrderListModel.class));

                    getDealerWiseListAdapter = new GetDealerWiseListAdapter(getContext(), DealerListModels);
                    spnProdtlist.setAdapter(getDealerWiseListAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
