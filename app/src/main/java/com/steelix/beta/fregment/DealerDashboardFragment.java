package com.steelix.beta.fregment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.steelix.beta.BaseFragment;
import com.steelix.beta.R;
import com.steelix.beta.activity.CompanyOffersActivity;
import com.steelix.beta.activity.EventActivity;
import com.steelix.beta.activity.NewOrderActivity;
import com.steelix.beta.activity.OrderListActivity;
import com.steelix.beta.activity.PreReportDetailActivity;
import com.steelix.beta.activity.SupportActivity;
import com.steelix.beta.adapter.OfferPagerSliderAdapter;
import com.steelix.beta.model.CompanyOfferModel;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscription;


public class DealerDashboardFragment extends BaseFragment {
    private static final String TAG = "DealerDashboardFragment";
    @BindView(R.id.dealer_Order)
    TextView dealerOrder;
    @BindView(R.id.dealer_Hist)
    TextView dealerHist;
    @BindView(R.id.company_Offer)
    TextView companyOffer;
    @BindView(R.id.company_Event)
    TextView companyEvent;
    @BindView(R.id.dealer_report)
    TextView dealerReport;
    @BindView(R.id.dealer_Contact)
    TextView dealerContact;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    ViewPagerIndicator indicator;

    private Unbinder unbinder;

    // DealerDashboardAdpater dashboardadpater;
    List<DisributorListModel> disributorListModels = new ArrayList<>();
    List<CompanyOfferModel> OfferList = new ArrayList<>();

    Subscription subscription;

    public DealerDashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dealer_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        indicator = view.findViewById(R.id.indicator);

        if (L.isNetworkAvailable(getContext())) {
            getOffer();

            if (prefs.getBoolean(Constant.isLogin, false)) {
                getDistList();
            } else {
                dealerOrder.setText("Products");
            }
        }


        return view;
    }


    @OnClick({R.id.dealer_Order, R.id.dealer_Hist, R.id.company_Offer, R.id.company_Event, R.id.dealer_report, R.id.dealer_Contact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.dealer_Order:
                startActivity(new Intent(getContext(), NewOrderActivity.class)
                        .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels));
                break;
            case R.id.dealer_Hist:
                if (prefs.getBoolean(Constant.isLogin, false)) {
                    startActivity(new Intent(getContext(), OrderListActivity.class));
                } else {
                    L.alert(getActivity(), "Kindly login to check your order history..!");
                }
                break;
            case R.id.company_Offer:
                startActivity(new Intent(getContext(), CompanyOffersActivity.class)
                        .putExtra("Type", "3"));
                break;
            case R.id.company_Event:
                startActivity(new Intent(getContext(), EventActivity.class));
                break;
            case R.id.dealer_report:

                if (prefs.getBoolean(Constant.isLogin, false)) {
                    startActivity(new Intent(getContext(), PreReportDetailActivity.class)
                            .putExtra("From", "Dealer"));
                } else {
                    L.alert(getActivity(), "Kindly login to check your Reports..!");
                }

                break;
            case R.id.dealer_Contact:
                startActivity(new Intent(getContext(), SupportActivity.class));
                break;

        }
    }

    private void getDistList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ID, user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getdistList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    disributorListModels.clear();
                    disributorListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DISTRIBUTORLIST).toString(), DisributorListModel.class));
                    //dashboardadpater.notifyDataSetChanged();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getOffer() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDashOfferList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    OfferList.clear();
                    OfferList.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.OFFERLIST).toString(), CompanyOfferModel.class));
                    Log.d(TAG, "getOffer list: " + OfferList.size());

                    pager.setAdapter(new OfferPagerSliderAdapter(getContext(), OfferList));
                    indicator.setupWithViewPager(pager);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getContext(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
