package com.steelix.beta.fregment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseFragment;
import com.steelix.beta.R;
import com.steelix.beta.activity.CompanyOffersActivity;
import com.steelix.beta.activity.DistributorOrderListActivity;
import com.steelix.beta.activity.DistributorReportActivity;
import com.steelix.beta.activity.EventActivity;
import com.steelix.beta.activity.PreReportDetailActivity;
import com.steelix.beta.adapter.EventListAdapter;
import com.steelix.beta.model.DistOrderListModel;
import com.steelix.beta.model.StatusListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscription;


public class DistributorDashboardFragment extends BaseFragment {

    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    @BindView(R.id.dist_Order)
    TextView distOrder;
    @BindView(R.id.dist_Report)
    TextView distReport;
    @BindView(R.id.dist_Compnyoffer)
    TextView distCompnyoffer;
    @BindView(R.id.dist_Compnyevent)
    TextView distCompnyevent;
    @BindView(R.id.tv_noOrder)
    TextView tvNoOrder;
    private Unbinder unbinder;

    EventListAdapter eventListAdapter;

    List<DistOrderListModel> MaindistOrderListModels = new ArrayList<>();
    List<StatusListModel> statusListModels = new ArrayList<>();

    public DistributorDashboardFragment() {
    }

    Subscription subscription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_distributor_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        eventListAdapter = new EventListAdapter(getActivity(), MaindistOrderListModels, statusListModels);
        recyclerlist.setAdapter(eventListAdapter);

        if (L.isNetworkAvailable(getContext())) {
            getStatusList();
            getDealerList();
        }

        return view;
    }


    @OnClick({R.id.dist_Order, R.id.dist_Report, R.id.dist_Compnyoffer, R.id.dist_Compnyevent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.dist_Order:
                startActivity(new Intent(getContext(), DistributorOrderListActivity.class));
                break;
            case R.id.dist_Report:
                startActivity(new Intent(getContext(), PreReportDetailActivity.class).putExtra("From", "Dist"));
                break;
            case R.id.dist_Compnyoffer:
                startActivity(new Intent(getContext(), CompanyOffersActivity.class).putExtra("Type","2"));
                break;
            case R.id.dist_Compnyevent:
                startActivity(new Intent(getContext(), EventActivity.class));
                break;
        }
    }

    private void getDealerList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DistributorID, user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDealerList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    MaindistOrderListModels.clear();
                    List<DistOrderListModel> distOrderListModels = LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DEALERLIST).toString(), DistOrderListModel.class);
                    if (distOrderListModels != null && distOrderListModels.size() > 0) {
                        String s = "", temps;
                        for (int i = 0; i < distOrderListModels.size(); i++) {
                            if (s.equals("")) {
                                s = L.getDate(distOrderListModels.get(i).getOrderDate());
                                distOrderListModels.get(i).setHeader(true);
                            }
                            temps = L.getDate(distOrderListModels.get(i).getOrderDate());
                            if ((!distOrderListModels.get(i).isHeader()) && s.equals(temps)) {
                                distOrderListModels.get(i).setHeader(false);
                            } else {
                                s = L.getDate(distOrderListModels.get(i).getOrderDate());
                                distOrderListModels.get(i).setHeader(true);
                            }
                        }
//                            messageList.clear();
                        MaindistOrderListModels.addAll(distOrderListModels);
                        eventListAdapter.notifyDataSetChanged();
                        if (MaindistOrderListModels.size() != 0) {
                            tvCount.setVisibility(View.VISIBLE);
                            tvCount.setText("" + MaindistOrderListModels.size());
                        }
                        if (MaindistOrderListModels.size() == 0) {
                            recyclerlist.setVisibility(View.GONE);
                            tvNoOrder.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getContext(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getStatusList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, user.getMobileNo());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getStatusList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    statusListModels.clear();
                    statusListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.STATUSLIST).toString(), StatusListModel.class));
                    //eventPicListAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getContext(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
