package com.steelix.beta.listner;

import com.steelix.beta.model.ProductsListModel;

public class RemoveProductEvent {

    int pos;
    ProductsListModel productsListModel;

    public RemoveProductEvent(int pos,ProductsListModel productsListModel) {
        this.pos = pos;
        this.productsListModel = productsListModel;
    }

    public int getPos() {
        return pos;
    }

    public ProductsListModel getProductsListModel() {
        return productsListModel;
    }
}
