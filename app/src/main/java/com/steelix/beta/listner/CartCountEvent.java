package com.steelix.beta.listner;

public class CartCountEvent {
    int count;
    public int getCount() {
        return count;
    }

    public CartCountEvent(int count) {
        this.count = count;
    }
}
