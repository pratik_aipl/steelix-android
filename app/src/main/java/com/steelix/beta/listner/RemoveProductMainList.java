package com.steelix.beta.listner;

import com.steelix.beta.model.ProductsListModel;

public interface RemoveProductMainList {
    void onRemoveProduct(ProductsListModel productsListModel, int pos);
}
