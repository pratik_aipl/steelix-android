package com.steelix.beta.listner;

public interface ChangeStatus {
    void onChangePStatus(int pos, int modelId, String status, int selectedListItemPos);
}
