package com.steelix.beta.listner;

public interface DialogButtonListener {
    void onPositiveButtonClicked();
    void onNegativButtonClicked();
}