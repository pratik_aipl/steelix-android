package com.steelix.beta.listner;

import com.steelix.beta.model.ProductsListModel;

public interface RemoveProduct {
    void onRemoveProduct(ProductsListModel productsListModel, int pos);
}
