package com.steelix.beta;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.steelix.beta.model.UserData;
import com.steelix.beta.network.RestAPIBuilder;
import com.steelix.beta.network.RestApi;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.Prefs;
import com.tbruyelle.rxpermissions2.RxPermissions;

public class BaseFragment extends Fragment {

    protected LinearLayoutManager layoutManager;

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    ProgressDialog progressDialog;
    public RxPermissions rxPermissions ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        restApi = RestAPIBuilder.buildRetrofitService();
        rxPermissions= new RxPermissions(this);

        prefs = Prefs.with(getActivity());
        gson = new Gson();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();

        }
    }
}