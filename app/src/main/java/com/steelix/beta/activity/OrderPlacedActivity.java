package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderPlacedActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_distname)
    TextView tvDistname;
    @BindView(R.id.tv_orderid)
    TextView tvOrderid;
    @BindView(R.id.tv_qty)
    TextView tvQty;
    @BindView(R.id.tv_dealer)
    TextView tvDealer;
    @BindView(R.id.btn_neword)
    Button btnNeword;
    @BindView(R.id.tv_remainDays)
    TextView tvRemainDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);
        ButterKnife.bind(this);

        tvDistname.setText(getIntent().getStringExtra(Constant.DISTRIBUTORNAME));
        tvOrderid.setText(getIntent().getStringExtra(Constant.ORDERNO));
        tvQty.setText(getIntent().getStringExtra(Constant.PQTY));
        tvDealer.setText(getIntent().getStringExtra(Constant.DEALERNAME));

        tvTittle.setText("Placed Order");
        tvRemainDays.setText("Order will be deliver by "+getIntent().getStringExtra(Constant.REMAINDAYS)+" Days to You ");
    }

    @OnClick({R.id.img_back, R.id.btn_neword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                startActivity(new Intent(OrderPlacedActivity.this, Dashboard.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.btn_neword:
                startActivity(new Intent(OrderPlacedActivity.this, Dashboard.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
        }
    }
}
