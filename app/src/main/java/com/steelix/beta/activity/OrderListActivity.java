package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.OrderListAdpater;
import com.steelix.beta.model.OrderListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class OrderListActivity extends BaseActivity {

    private static final String TAG = "OrderListActivity";
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    OrderListAdpater orderListAdpater;
    List<OrderListModel> orderListModelsMain = new ArrayList<>();
    Subscription subscription;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        tvTittle.setText("Order List");
        if (L.isNetworkAvailable(this)) {
            getOrderList();
        }
        orderListAdpater = new OrderListAdpater(this, orderListModelsMain);
        recyclerList.setAdapter(orderListAdpater);


        swipeContainer.setOnRefreshListener(() -> {
            getOrderList();
            swipeContainer.setRefreshing(false);
        });

    }

    private void getOrderList() {
        Map<String, String> map = new HashMap<>();
        map.put("DealerId", user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrdelList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    orderListModelsMain.clear();
                    List<OrderListModel> orderListModels = LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.ORDER_LIST).toString(), OrderListModel.class);
                    if (orderListModels != null && orderListModels.size() > 0) {
                        String s = "", temps;
                        for (int i = 0; i < orderListModels.size(); i++) {
                            if (s.equals("")) {
                                s = L.getDate(orderListModels.get(i).getOrderDate());
                                orderListModels.get(i).setHeader(true);
                            }
                            temps = L.getDate(orderListModels.get(i).getOrderDate());
                            if ((!orderListModels.get(i).isHeader()) && s.equals(temps)) {
                                orderListModels.get(i).setHeader(false);
                            } else {
                                s = L.getDate(orderListModels.get(i).getOrderDate());
                                orderListModels.get(i).setHeader(true);
                            }
                        }
//                            messageList.clear();
                        orderListModelsMain.addAll(orderListModels);
                        orderListAdpater.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
