package com.steelix.beta.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.DealerReportAdpater;
import com.steelix.beta.model.DealerReportListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.GeneratePdfClass;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.steelix.beta.utils.GeneratePdfClass.getRecyclerViewScreenshot;

public class DealerReportActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_toDate)
    TextView tvToDate;
    @BindView(R.id.spn_Prodtlist)
    Spinner spnProdtlist;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    List<DealerReportListModel> dealerReportListModels = new ArrayList<>();
    DealerReportAdpater dealerReportAdpater;

    Subscription subscription;
    final Calendar myCalendar = Calendar.getInstance();

    @BindView(R.id.tv_fromDate)
    TextView tvFromDate;

    DProductListAdapter dproductListAdapter;
    List<ProductsListModel> productsListModels = new ArrayList<>();
    String ToDATE = "", FromDATE = "", ProductId = "";
    @BindView(R.id.tv_notavl)
    TextView tvNotavl;
    DatePickerDialog datePickerDialog;
    @BindView(R.id.export_pdf)
    FloatingActionButton exportPdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_report);
        ButterKnife.bind(this);
        tvTittle.setText("Reports");

        dproductListAdapter = new DProductListAdapter(this, productsListModels);
        spnProdtlist.setAdapter(dproductListAdapter);

        if (L.isNetworkAvailable(this)) {
            getDealerReport();
            getProductList();
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        dealerReportAdpater = new DealerReportAdpater(this, dealerReportListModels);
        recyclerlist.setAdapter(dealerReportAdpater);

        spnProdtlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Toast.makeText(DealerReportActivity.this, "Please Select Product", Toast.LENGTH_SHORT).show();
                } else {
                    ProductId = String.valueOf(productsListModels.get(position - 1).getProductID());
                    getDealerReport();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        exportPdf.setOnClickListener(view -> {
            rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribe(granted -> {
                        if (granted) {
                            GeneratePdfClass.createPdf(getRecyclerViewScreenshot(recyclerlist), this);
                        } else {
                            // At least one permission is denied
                        }
                    });
        });
    }


    @OnClick({R.id.img_back, R.id.tv_toDate, R.id.tv_fromDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_toDate:
                datePickerDialog = new DatePickerDialog(DealerReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;
            case R.id.tv_fromDate:
                datePickerDialog = new DatePickerDialog(DealerReportActivity.this, fromdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;
        }
    }

    private void toDate() {
        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvToDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        ToDATE = sdf.format(myCalendar.getTime());

    }

    private void fromDate() {
        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvFromDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        FromDATE = sdf.format(myCalendar.getTime());

    }

    DatePickerDialog.OnDateSetListener fromdate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        fromDate();
    };
    DatePickerDialog.OnDateSetListener todate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        toDate();
    };

    private void getDealerReport() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DEALERID, user.getId());
        map.put(Constant.FROM, FromDATE);
        map.put(Constant.TO, ToDATE);
        map.put(Constant.ProductID, ProductId);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDealerReport(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    dealerReportListModels.clear();
                    dealerReportListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.REPORTLIST).toString(), DealerReportListModel.class));
                    dealerReportAdpater.notifyDataSetChanged();
                    if (dealerReportListModels.size() <= 0) {
                        tvNotavl.setVisibility(View.VISIBLE);
                        recyclerlist.setVisibility(View.GONE);
                        exportPdf.hide();
                    } else {
                        tvNotavl.setVisibility(View.GONE);
                        recyclerlist.setVisibility(View.VISIBLE);
                        exportPdf.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getProductList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getProduct(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    productsListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.PRODUCTLIST).toString(), ProductsListModel.class));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}

class DProductListAdapter extends BaseAdapter {
    List<ProductsListModel> productsListModel;
    LayoutInflater inflter;
    Context context;

    public DProductListAdapter(Context context, List<ProductsListModel> productsListModel) {
        this.context = context;
        this.productsListModel = productsListModel;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return productsListModel.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Products");
            names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));

        } else {
            ProductsListModel productsModel = productsListModel.get(position - 1);
            names.setText(productsModel.getProductName());
            //  names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}
