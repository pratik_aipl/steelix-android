package com.steelix.beta.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.fregment.DealerDashboardFragment;
import com.steelix.beta.fregment.DistributorDashboardFragment;
import com.steelix.beta.listner.CartCountEvent;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscription;

public class Dashboard extends BaseActivity {

    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.des_tittle)
    TextView desTittle;
    ActionBarDrawerToggle t;
    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.navi_view)
    NavigationView naviView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    //String logtype = "3";
    @BindView(R.id.tv_mailid)
    TextView tvMailid;
    @BindView(R.id.tv_mob)
    TextView tvMob;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.btn_login)
    Button btnlogin;
    @BindView(R.id.tv_count)
    TextView tvCount;
    Subscription subscription;
    @BindView(R.id.img_Profile)
    CircleImageView imgProfile;

    List<DisributorListModel> disributorListModels = new ArrayList<>();
    List<ProductsListModel> cartProductList = new ArrayList<>();
    @BindView(R.id.Distributor)
    LinearLayout Distributor;
    @BindView(R.id.rel_main)
    RelativeLayout relMain;
    @BindView(R.id.img_Pedit)
    ImageView imgPedit;

    @BindView(R.id.Dashboard)
    LinearLayout Dashboard;
    @BindView(R.id.support)
    LinearLayout support;
    @BindView(R.id.tc)
    LinearLayout tc;
    @BindView(R.id.report)
    LinearLayout report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (prefs.getBoolean(Constant.isLogin, false)) {

            if (user.getRoleid().equalsIgnoreCase("3")) {
                selectItem(0);
                tvType.setText("I Am Dealer");

            } else {
                selectItem(1);
                tvType.setText("I Am Distributor");
                Distributor.setVisibility(View.GONE);
            }

            tvMailid.setText(user.getEmailID());
            tvMob.setText(user.getMobileNo());

            if (user.getProfile() != null) {
                L.loadImageWithPicasso(Dashboard.this, user.getProfile(), imgProfile, null);
            }
            btnLogout.setVisibility(View.VISIBLE);
        } else {
            selectItem(0);
            tvType.setText("I Am Guest");

            tvMailid.setVisibility(View.GONE);
            tvMob.setVisibility(View.GONE);
            btnLogout.setVisibility(View.GONE);

            Distributor.setVisibility(View.GONE);
            report.setVisibility(View.GONE);
            imgPedit.setVisibility(View.GONE);
            imgCart.setVisibility(View.GONE);

            btnlogin.setVisibility(View.VISIBLE);
            btnlogin.setOnClickListener(v -> {
                startActivity(new Intent(Dashboard.this, LoginActivity.class));

            });
        }


        desTittle.setText("Dashboard");
        drawerLayout.addDrawerListener(t);


        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            List<ProductsListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
            if (productsListModels.size() > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(getString(R.string.qty, productsListModels.size()));
            } else
                tvCount.setVisibility(View.GONE);
        }
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new DealerDashboardFragment();
                break;
            case 1:
                fragment = new DistributorDashboardFragment();
                imgCart.setVisibility(View.GONE);
                break;

            default:
                break;
        }

        if (fragment != null) {
            // drawerLayout.closeDrawer(Gravity.LEFT);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.framelayout, fragment).commit();
        } else {
            Log.e("Dash", "Error in creating fragment");

        }
    }

    @OnClick({R.id.img_drawer, R.id.img_cart, R.id.img_Pedit, R.id.Dashboard, R.id.Distributor, R.id.btn_logout, R.id.support, R.id.tc, R.id.report})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawer:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.img_cart:
                getDistList();
                break;
            case R.id.img_Pedit:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(Dashboard.this, EditProfileActivity.class));
                break;
            case R.id.Dashboard:
                drawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case R.id.support:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(Dashboard.this, SupportActivity.class));
                break;
            case R.id.Distributor:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(Dashboard.this, DistributorListActivity.class));
                break;
            case R.id.tc:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(Dashboard.this, TermsandCondition.class));
                break;
            case R.id.report:
                drawerLayout.closeDrawer(Gravity.LEFT);
                if (user.getRoleid().equalsIgnoreCase("3")) {
                    startActivity(new Intent(Dashboard.this, PreReportDetailActivity.class)
                            .putExtra("From", "Dealer"));
                } else {
//                    DistributorReportActivity
                    startActivity(new Intent(Dashboard.this, PreReportDetailActivity.class)
                            .putExtra("From", "Dist"));
                }
                break;
            case R.id.btn_logout:
                alert();
                break;
        }
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage((R.string.logout))
                .setPositiveButton("YES", (dialog, which) -> {
                    getLogout();
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void getLogout() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogout(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    L.logout(Dashboard.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CartCountEvent event) {
        //cartProductList.remove(event.getPos());
        if (event.getCount() > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(getString(R.string.qty, event.getCount()));
        } else {
            tvCount.setVisibility(View.GONE);
        }
    }

    private void getDistList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ID, user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getdistList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    disributorListModels.clear();
                    disributorListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DISTRIBUTORLIST).toString(), DisributorListModel.class));
                    // dashboardadpater.notifyDataSetChanged();
                    // if (disributorListModels.size() > 0) {
                    startActivity(new Intent(Dashboard.this, CartActivity.class)
                            .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels));
                   /* } else {
                        Toast.makeText(this, "Something goes to wrong", Toast.LENGTH_SHORT).show();
                    }
*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
