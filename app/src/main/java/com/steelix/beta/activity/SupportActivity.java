package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.steelix.beta.BuildConfig;
import com.steelix.beta.R;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SupportActivity extends AppCompatActivity {

    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_Cright)
    TextView tvCright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);
        tvTittle.setText("Support");
        tvVersion.setText("Version " + BuildConfig.VERSION_NAME);
        tvCright.setText("Copyright" + Calendar.getInstance().get(Calendar.YEAR) + " Steelix co.");
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
