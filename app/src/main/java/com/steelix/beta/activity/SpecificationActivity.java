package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpecificationActivity extends BaseActivity {

    @BindView(R.id.img_product)
    ImageView imgProduct;
    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.des_tittle)
    TextView desTittle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_proName)
    TextView tvProName;
    @BindView(R.id.tv_seriesName)
    TextView tvSeriesName;
    @BindView(R.id.tv_capacity)
    TextView tvCapacity;
    @BindView(R.id.tv_keyfeature)
    TextView tvKeyfeature;
    ProductsListModel productsListModel;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.rel_main)
    RelativeLayout relMain;
    List<DisributorListModel> disributorListModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specification);
        ButterKnife.bind(this);
        imgDrawer.setImageResource(R.drawable.back_arrow);
        desTittle.setText("Product Details");


        productsListModel = (ProductsListModel) getIntent().getSerializableExtra(Constant.selectedProduct);
        disributorListModels = (List<DisributorListModel>) getIntent().getSerializableExtra(Constant.DISTRIBUTORLIST);

        if (productsListModel != null) {
            // selectedProducts = productsListModels.get(position - 1);
            if (productsListModel.getProduct_image() != null) {
                if (productsListModel.getProduct_image().size() > 0)
                    L.loadImageWithPicasso(SpecificationActivity.this, productsListModel.getProduct_image().get(0).getImage(), imgProduct, null);
            }
            tvProName.setText(":  " + productsListModel.getProductName());
            tvCapacity.setText(":  " + productsListModel.getCapacity());
            tvSeriesName.setText(":  " + productsListModel.getSkucode());
            tvKeyfeature.setText(":  " + productsListModel.getProductName());
            tvDesc.setText(Html.fromHtml(productsListModel.getDescription()));

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            List<ProductsListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
            if (productsListModels.size() > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(getString(R.string.qty, productsListModels.size()));
            } else
                tvCount.setVisibility(View.GONE);
        } else {
            tvCount.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.img_drawer, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawer:
                onBackPressed();
                break;
            case R.id.img_cart:
                startActivity(new Intent(SpecificationActivity.this, CartActivity.class)
                        .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels));
                break;
        }
    }
}
