package com.steelix.beta.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.BuildConfig;
import com.steelix.beta.R;
import com.steelix.beta.model.UserData;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.network.RestApi;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;
import com.steelix.beta.utils.Utility;
import com.steelix.beta.utils.Validation;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static com.steelix.beta.utils.ImagePathUtils.getFilePath;
import static com.steelix.beta.utils.L.onCaptureImageResult;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.img_EDTProfile)
    CircleImageView imgEDTProfile;
    @BindView(R.id.edt_Fname)
    EditText edtFname;
    @BindView(R.id.edt_Lname)
    EditText edtLname;
    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_compnyname)
    EditText edtCompnyname;
    @BindView(R.id.edt_address)
    EditText edtAddress;

    String userChoosenTask;
    File imgFile = null;
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    String mCurrentPhotoPath;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        tvTittle.setText("Edit Profile");

        edtFname.setText(user.getFirstName());
        edtLname.setText(user.getLastName());
        edtMobileno.setText(user.getMobileNo());
        edtMobileno.setEnabled(false);
        edtEmail.setText(user.getEmailID());
        edtAddress.setText(user.getAddress());

        if (user.getProfile() != null) {
            L.loadImageWithPicasso(EditProfileActivity.this, user.getProfile(), imgEDTProfile, null);
        }
    }

    @OnClick({R.id.img_back, R.id.btn_update, R.id.img_EDTProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_update:
                if (Validation.isEmpty(L.getEditText(edtFname))) {
                    edtFname.setError("Please enter FirstName");
                } else if (Validation.isEmpty(L.getEditText(edtLname))) {
                    edtLname.setError("Please enter LastName");
                } else if (Validation.isEmpty(L.getEditText(edtEmail))) {
                    edtEmail.setError("Please enter EmailID");
                } else if (Validation.isEmpty(L.getEditText(edtAddress))) {
                    edtAddress.setError("Please enter Address");
                } else if (!TextUtils.isEmpty(edtEmail.getText()) && !Validation.isValidEmail(edtEmail.getText())) {
                    edtEmail.setError("Check Email ID");
                } else {
                    updateProfile(L.getEditText(edtFname),
                            L.getEditText(edtLname),
                            L.getEditText(edtEmail),
                            L.getEditText(edtMobileno),
                            L.getEditText(edtAddress));
                }
                break;
            case R.id.img_EDTProfile:
                selectImage();
                break;
        }
    }


    private void updateProfile(String fname, String lname, String email, String mobile, String address) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.FNAME, RestApi.createRequestBody(fname));
        map.put(Constant.LName, RestApi.createRequestBody(lname));
        map.put(Constant.EMAIL, RestApi.createRequestBody(email));
        map.put(Constant.USERID, RestApi.createRequestBody(user.getUserID()));
        map.put(Constant.ADDRESS, RestApi.createRequestBody(address));

        MultipartBody.Part body = null;

        if (imgFile != null && !TextUtils.isEmpty(imgFile.getAbsolutePath())) {
            RequestBody requestFile = null;
            try {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new Compressor(this).compressToFile(imgFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            body = MultipartBody.Part.createFormData(Constant.ProfileImage, imgFile.getName(), requestFile);
        }

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.updateProfile(map, body), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    Toast.makeText(this, "" + jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean result = Utility.checkPermission(EditProfileActivity.this);

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                imgFile = createImageFile();
                if (imgFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            imgFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(imgFile.getAbsolutePath(), imgEDTProfile);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imgFile = new File(Objects.requireNonNull(getFilePath(this, data.getData())));
        imgEDTProfile.setImageBitmap(bm);
    }
}
