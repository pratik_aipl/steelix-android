package com.steelix.beta.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.OrderDetailsAdpater;
import com.steelix.beta.model.OrderDetailListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class OrderDetailsActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_deliver_name)
    TextView tvDeliverName;
    @BindView(R.id.tv_orderDate)
    TextView tvOrderDate;
    @BindView(R.id.tv_orderNo)
    TextView tvOrderNo;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    @BindView(R.id.tv_total_qty)
    TextView tvTotalQty;

    OrderDetailsAdpater orderDetailsAdpater;
    List<OrderDetailListModel> orderDetailListModels = new ArrayList<>();

    Subscription subscription;
    int OrderID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);

        tvTittle.setText("Order Details");
        OrderID = getIntent().getIntExtra("OrderID", 0);

        /*  .putExtra("OrderID", orderListModel.getOrderID())
                    .putExtra("OrderDate", orderListModel.getOrderDate())
                    .putExtra("StatusColour", orderListModel.getStatusColorCode())
                    .putExtra("Status", orderListModel.getStatusCode())
                    .putExtra("OrderNo", orderListModel.getOrderID()));
                    */

        tvOrderDate.setText("Order Date: " + " " + L.getDate(getIntent().getStringExtra("OrderDate")));
        tvOrderNo.setText("Order No.: " + " " + getIntent().getStringExtra("OrderNo"));
        tvStatus.setText(getIntent().getStringExtra("Status"));
        tvStatus.setBackground(L.changeDrawableColor(OrderDetailsActivity.this, R.drawable.button, Color.parseColor(getIntent().getStringExtra("StatusColour"))));


        if (L.isNetworkAvailable(this))
            getOrderDetails(OrderID);

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        orderDetailsAdpater = new OrderDetailsAdpater(this, orderDetailListModels);
        recyclerlist.setAdapter(orderDetailsAdpater);
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    private void getOrderDetails(int orderID) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.OrderID, String.valueOf(orderID));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrderDetails(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    orderDetailListModels.clear();
                    orderDetailListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.ORDER_LIST).toString(), OrderDetailListModel.class));
                    orderDetailsAdpater.notifyDataSetChanged();
                    int totalqty = 0;
                    for (int i = 0; i < orderDetailListModels.size(); i++) {
                        totalqty = totalqty + orderDetailListModels.get(i).getQty();
                    }
                    tvTotalQty.setText(getString(R.string.total_qty, totalqty));
                       tvDeliverName.setText("Order Will be Deliver " + " " +jsonResponse.getJSONObject(Constant.data).getJSONArray("deleery By").getJSONObject(0).getString("full_name"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
