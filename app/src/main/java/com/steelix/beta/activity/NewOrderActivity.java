package com.steelix.beta.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.listner.CartCountEvent;
import com.steelix.beta.listner.RemoveProductEvent;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class NewOrderActivity extends BaseActivity {
    private static final String TAG = "NewOrderActivity";
    @BindView(R.id.des_tittle)
    TextView desTittle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.tv_specification)
    TextView tvSpecification;
    @BindView(R.id.img_Qplus)
    ImageView imgQplus;
    @BindView(R.id.tv_Qcount)
    TextView tvQcount;
    @BindView(R.id.img_Qminus)
    ImageView imgQminus;
    @BindView(R.id.btn_addtocart)
    TextView btnAddtocart;
    @BindView(R.id.btn_buynow)
    TextView btnBuyNow;
    int minteger = 1;

    Subscription subscription;
    List<ProductsListModel> productsListModels = new ArrayList<>();
    List<ProductsListModel> cartProductList = new ArrayList<>();
    @BindView(R.id.spn_product)
    Spinner spnProduct;
    @BindView(R.id.li_product)
    LinearLayout liProduct;
    ProductListAdapter productListAdapter;
    @BindView(R.id.li_main)
    LinearLayout liMain;
    @BindView(R.id.img_product)
    ImageView imgProduct;
    @BindView(R.id.tv_PName)
    TextView tvPName;
    @BindView(R.id.tv_Pcapacity)
    TextView tvPcapacity;
    @BindView(R.id.tv_PSpeci)
    TextView tvPSpeci;
    int selectedPos = -1;
    //   ProductsListModel selectedProducts = null;
    @BindView(R.id.tv_count)
    TextView tvCount;
    List<DisributorListModel> disributorListModels = new ArrayList<>();
    @BindView(R.id.btn_logintobuy)
    TextView btnLogintobuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        ButterKnife.bind(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (!prefs.getBoolean(Constant.isLogin, false)) {
            btnLogintobuy.setVisibility(View.VISIBLE);
            btnBuyNow.setVisibility(View.GONE);
            btnAddtocart.setVisibility(View.GONE);
            imgCart.setVisibility(View.GONE);
            desTittle.setText("Products");

        } else {
            desTittle.setText("New Order");
        }


        imgDrawer.setImageResource(R.drawable.back_arrow);
        imgDrawer.setOnClickListener(view -> onBackPressed());
        tvSpecification.setPaintFlags(tvSpecification.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        disributorListModels = (List<DisributorListModel>) getIntent().getSerializableExtra(Constant.DISTRIBUTORLIST);


        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            cartProductList.addAll(new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type)));
            if (cartProductList.size() != 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(getString(R.string.qty, cartProductList.size()));
            } else {
                tvCount.setVisibility(View.GONE);
            }

        }

        productListAdapter = new ProductListAdapter(this, productsListModels);
        spnProduct.setAdapter(productListAdapter);

        spnProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    liMain.setVisibility(View.GONE);
                    //  btnAddtocart.setClickable(false);
                    //  btnBuyNow.setClickable(false);
                    minteger = 1;
                } else {
                    if (productsListModels.get(position - 1).getProduct_image() != null) {
                        if (productsListModels.get(position - 1).getProduct_image().size() > 0)
                            L.loadImageWithPicasso(NewOrderActivity.this, productsListModels.get(position - 1).getProduct_image().get(0).getImage(), imgProduct, null);
                    }
                    selectedPos = (position - 1);
                    ProductsListModel selectedProducts = productsListModels.get(position - 1);
                    if (selectedProducts.getCartQty() > 0) {
                        tvQcount.setText(getString(R.string.qty, selectedProducts.getCartQty()));
                    } else {
                        tvQcount.setText(getString(R.string.qty, 1));
                    }
                    liMain.setVisibility(View.VISIBLE);
                    tvPName.setText(selectedProducts.getProductName());
                    tvPcapacity.setText(selectedProducts.getCapacity());
                    tvPSpeci.setText(selectedProducts.getSpecification());
                    btnAddtocart.setClickable(true);
                    btnBuyNow.setClickable(true);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (L.isNetworkAvailable(NewOrderActivity.this)) {
            getProductList();
        }
    }

    @OnClick({R.id.img_Qplus, R.id.img_Qminus, R.id.btn_addtocart, R.id.btn_buynow, R.id.tv_specification, R.id.img_cart, R.id.btn_logintobuy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_Qplus:
                if (productsListModels.get(selectedPos).getCartQty() > 0) {
                    minteger = productsListModels.get(selectedPos).getCartQty();
                } else {
                    minteger = 1;
                }
                minteger = minteger + 1;
                productsListModels.get(selectedPos).setCartQty(minteger);
                productListAdapter.notifyDataSetChanged();

                tvQcount.setText(getString(R.string.qty, minteger));
                break;
            case R.id.img_Qminus:
                if (productsListModels.get(selectedPos).getCartQty() > 0) {
                    minteger = productsListModels.get(selectedPos).getCartQty();
                } else {
                    minteger = 1;
                }
                if (minteger > 1)
                    minteger = minteger - 1;
                productsListModels.get(selectedPos).setCartQty(minteger);
                productListAdapter.notifyDataSetChanged();
                tvQcount.setText(getString(R.string.qty, minteger));
                break;
            case R.id.btn_addtocart:
                Log.d(TAG, "onViewClicked: " + productsListModels.size());
                if (selectedPos != -1) {
                    ProductsListModel productsListModel = productsListModels.get(selectedPos);
                    if (productsListModel.isFromCart()) {
                        Toast.makeText(this, "Cart Updated.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Product successfully add.", Toast.LENGTH_SHORT).show();
                    }
                    productsListModel.setCartQty(minteger);
                    productsListModels.set(selectedPos, productsListModel);
                    productListAdapter.notifyDataSetChanged();

                    cartProductList.clear();
                    for (int i = 0; i < productsListModels.size(); i++) {
                        if (productsListModels.get(i).getCartQty() > 0)
                            cartProductList.add(productsListModels.get(i));
                    }
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(getString(R.string.qty, cartProductList.size()));
                    spnProduct.setSelection(0);

                    prefs.save(Constant.cartData, "");
                    prefs.save(Constant.cartData, gson.toJson(cartProductList));
                    EventBus.getDefault().post(new CartCountEvent(cartProductList.size()));
                } else {
                    Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_buynow:
                Log.d(TAG, "onViewClicked: " + productsListModels.size());
                if (selectedPos != -1) {
                    ProductsListModel productsListModel = productsListModels.get(selectedPos);
                  /*  if (productsListModel.isFromCart()) {
                        Toast.makeText(this, "Cart Updated.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Product successfully add.", Toast.LENGTH_SHORT).show();
                    }*/
                    productsListModel.setCartQty(minteger);
                    productsListModels.set(selectedPos, productsListModel);
                    productListAdapter.notifyDataSetChanged();

                    cartProductList.clear();
                    for (int i = 0; i < productsListModels.size(); i++) {
                        if (productsListModels.get(i).getCartQty() > 0)
                            cartProductList.add(productsListModels.get(i));
                    }
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(getString(R.string.qty, cartProductList.size()));
                    spnProduct.setSelection(0);

                    prefs.save(Constant.cartData, "");
                    prefs.save(Constant.cartData, gson.toJson(cartProductList));
                    EventBus.getDefault().post(new CartCountEvent(cartProductList.size()));

                    startActivity(new Intent(NewOrderActivity.this, CartActivity.class)
                            .putExtra(Constant.selectedProduct, (Serializable) cartProductList)
                            .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels));
                } else {
                    Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_cart:
                //if (cartProductList.size() > 0) {
                startActivity(new Intent(NewOrderActivity.this, CartActivity.class)
                        .putExtra(Constant.selectedProduct, (Serializable) cartProductList)
                        .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels));
                // }
                break;
            case R.id.btn_logintobuy:
                startActivity(new Intent(NewOrderActivity.this, LoginActivity.class));
                break;
            case R.id.tv_specification:
                if (selectedPos != -1) {
                    startActivity(new Intent(NewOrderActivity.this, SpecificationActivity.class)
                            .putExtra(Constant.DISTRIBUTORLIST, (Serializable) disributorListModels)
                            .putExtra(Constant.selectedProduct, productsListModels.get(selectedPos)));
                }
                break;
        }
    }

    private void getProductList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getProduct(map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    productsListModels.clear();
                    productsListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.PRODUCTLIST).toString(), ProductsListModel.class));

                    for (int i = 0; i < productsListModels.size(); i++) {
                        for (int j = 0; j < cartProductList.size(); j++) {
                            if (productsListModels.get(i).getProductID() == cartProductList.get(j).getProductID()) {
                                productsListModels.get(i).setCartQty(cartProductList.get(j).getCartQty());
                                productsListModels.get(i).setFromCart(true);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemoveProductEvent event) {
        cartProductList.remove(event.getPos());
        if (cartProductList.size() > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(getString(R.string.qty, cartProductList.size()));
        } else {
            tvCount.setVisibility(View.GONE);
        }

        prefs.save(Constant.cartData, gson.toJson(cartProductList));
        Toast.makeText(this, event.getProductsListModel().getProductName() + " Product Remove.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}

class ProductListAdapter extends BaseAdapter {
    List<ProductsListModel> productsListModel;
    LayoutInflater inflter;
    Context context;

    public ProductListAdapter(Context context, List<ProductsListModel> productsListModel) {
        this.context = context;
        this.productsListModel = productsListModel;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return productsListModel.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Product");
            names.setTextColor(Color.GRAY);
        } else {
            ProductsListModel productsModel = productsListModel.get(position - 1);
            names.setText(productsModel.getProductName());
            //  names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}
