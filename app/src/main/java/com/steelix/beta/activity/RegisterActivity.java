package com.steelix.beta.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.model.CityListModel;
import com.steelix.beta.model.StateListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;
import com.steelix.beta.utils.Validation;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class RegisterActivity extends BaseActivity {

    private static final String TAG = "RegisterActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_join)
    LinearLayout btnJoin;
    String logtype;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_tc)
    TextView tvTc;
    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_compnyname)
    EditText edtCompnyname;
    @BindView(R.id.spn_state)
    Spinner spnState;
    @BindView(R.id.spn_city)
    Spinner spnCity;
    @BindView(R.id.edt_address)
    EditText edtAddress;
    @BindView(R.id.chk_condition)
    CheckBox chkCondition;

    Subscription subscription;
    List<StateListModel> stateListModels = new ArrayList<>();
    List<CityListModel> cityListModels = new ArrayList<>();

    StateListAdapter stateListAdapter;
    CityListAdapter cityListAdapter;
    @BindView(R.id.edt_Fname)
    EditText edtFname;
    @BindView(R.id.edt_Lname)
    EditText edtLname;
    int StateId, cityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        tvTittle.setText("Registration");
        logtype = getIntent().getStringExtra(Constant.logtype);
        tvTc.setPaintFlags(tvTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (L.isNetworkAvailable(this))
            getState();

        stateListAdapter = new StateListAdapter(this, stateListModels);
        spnState.setAdapter(stateListAdapter);

        cityListAdapter = new CityListAdapter(this, cityListModels);
        spnCity.setAdapter(cityListAdapter);

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {

                    getCity(stateListModels.get(position - 1).getStateid());
                    StateId = stateListModels.get(position - 1).getStateid();
                    // Toast.makeText(RegisterActivity.this, "" + StateId, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    cityId = cityListModels.get(position - 1).getCitiesid();
                // Toast.makeText(RegisterActivity.this, "" + StateId, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @OnClick({R.id.btn_join, R.id.img_back, R.id.tv_tc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_join:
                validation();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_tc:
                startActivity(new Intent(RegisterActivity.this, TermsandCondition.class));
                break;
        }
    }

    public void validation() {
        if (Validation.isEmpty(L.getEditText(edtFname))) {
            edtFname.setError("Please enter First Name");
        } else if (edtFname.getText().length() < 2) {
            edtFname.setError("Please enter minimum 2 character");
        } else if (Validation.isEmpty(L.getEditText(edtLname))) {
            edtLname.setError("Please enter Last Name");
        } else if (edtLname.getText().length() < 2) {
            edtLname.setError("Please enter minimum 2 character");
        } else if (Validation.isEmpty(L.getEditText(edtMobileno))) {
            edtMobileno.setError("Please enter Mobile No");
        } else if (!Validation.isValidPhoneNumber(edtMobileno.getText())) {
            edtMobileno.setError("Check Mobile No");
        } else if (Validation.isEmpty(L.getEditText(edtEmail))) {
            edtEmail.setError("Please enter Email ID");
        } else if (!TextUtils.isEmpty(edtEmail.getText()) && !Validation.isValidEmail(edtEmail.getText())) {
            edtEmail.setError("Check Email ID");
        } else if (stateListModels.size() == 0 || spnState.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select State", Toast.LENGTH_SHORT).show();
        } else if (cityListModels.size() == 0 || spnCity.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select City", Toast.LENGTH_SHORT).show();
        } else if (Validation.isEmpty(L.getEditText(edtAddress))) {
            edtAddress.setError("Please enter Address");
        } else if (!chkCondition.isChecked()) {
            Toast.makeText(this, "Please Agree Terms & Conditions", Toast.LENGTH_SHORT).show();
        } else {
            if (L.isNetworkAvailable(RegisterActivity.this)) {
                ConfirmUser();
            }
        }
    }

    private void getState() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getState(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    stateListModels.clear();
                    stateListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("state").toString(), StateListModel.class));
                    stateListAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getCity(int id) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.stateid, String.valueOf(id));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getCity(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    cityListModels.clear();
                    cityListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("cities").toString(), CityListModel.class));
                    cityListAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void ConfirmUser() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, (L.getEditText(edtMobileno)));
        map.put(Constant.roleid, logtype);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getUserRegVerify(map), (data) -> {
            showProgress(false);


            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject(Constant.data);
                    String otp = phone.getString("otp");
                    Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                    // intent.putExtra(Constant.logtype, logtype);
                    intent.putExtra(Constant.Otp, otp);

                    // intent.putExtra(Constant.MOBILENO, L.getEditText(edtMobileno));
                    intent.putExtra(Constant.IsFrom, "Register");

                    intent.putExtra(Constant.FNAME, L.getEditText(edtFname));
                    intent.putExtra(Constant.LName, L.getEditText(edtLname));
                    intent.putExtra(Constant.MOBILE, L.getEditText(edtMobileno));
                    intent.putExtra(Constant.EMAIL, L.getEditText(edtEmail));
                    intent.putExtra(Constant.COMPANYNAME, L.getEditText(edtCompnyname));
                    intent.putExtra(Constant.STATE, StateId);
                    intent.putExtra(Constant.CITY, cityId);
                    intent.putExtra(Constant.ADDRESS, L.getEditText(edtCompnyname));
                    intent.putExtra(Constant.ROLEID, logtype);


                    /*intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.code() == 206) {
                Toast.makeText(this, "Mobile No Already Registered...", Toast.LENGTH_SHORT).show();
            } else {
                L.serviceStatusFalseProcess(this, data);
            }
        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}

class StateListAdapter extends BaseAdapter {
    List<StateListModel> stateListModels;
    LayoutInflater inflter;
    Context context;

    public StateListAdapter(Context context, List<StateListModel> stateListModels) {
        this.context = context;
        this.stateListModels = stateListModels;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return stateListModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("State");
            names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));
        } else {
            StateListModel stateListModel = stateListModels.get(position - 1);
            names.setText(stateListModel.getStatename());
            //  names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}

class CityListAdapter extends BaseAdapter {
    List<CityListModel> cityListModels;
    LayoutInflater inflter;
    Context context;

    public CityListAdapter(Context context, List<CityListModel> cityListModels) {
        this.context = context;
        this.cityListModels = cityListModels;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return cityListModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("City");
            names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));
        } else {
            CityListModel cityListModel = cityListModels.get(position - 1);
            names.setText(cityListModel.getCitiesname());
            //names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}
