package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.CompanyOfferAdpater;
import com.steelix.beta.model.CompanyOfferModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class CompanyOffersActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    CompanyOfferAdpater companyOfferAdpater;
    List<CompanyOfferModel> companyOfferModels = new ArrayList<>();
    Subscription subscription;
    @BindView(R.id.tv_NoOffer)
    TextView tvNoOffer;
    String From;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_offers);
        ButterKnife.bind(this);
        tvTittle.setText("Company Offer");
        From = getIntent().getStringExtra("Type");
        if (L.isNetworkAvailable(this)) {
            getOffer(From);
        }


        swipeContainer.setOnRefreshListener(() -> {
            getOffer(From);
            swipeContainer.setRefreshing(false);
        });
        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        companyOfferAdpater = new CompanyOfferAdpater(this, companyOfferModels);
        recyclerlist.setAdapter(companyOfferAdpater);
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    private void getOffer(String from) {
        Map<String, String> map = new HashMap<>();
        map.put("type", from);
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOfferList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    companyOfferModels.clear();
                    companyOfferModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.OFFERLIST).toString(), CompanyOfferModel.class));
                    companyOfferAdpater.notifyDataSetChanged();
                   /* if (companyOfferModels.isEmpty()) {
                        recyclerlist.setVisibility(View.GONE);
                        tvNoOffer.setVisibility(View.VISIBLE);
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
