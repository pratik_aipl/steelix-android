package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.PreReportAdpater;
import com.steelix.beta.model.ReportPreListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class PreReportDetailActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    Subscription subscription;

    PreReportAdpater preReportAdpater;
    List<ReportPreListModel> reportPreListModels = new ArrayList<>();
    @BindView(R.id.btn_cutomize)
    TextView btnCutomize;

    String From;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_report_detail);
        ButterKnife.bind(this);
        tvTittle.setText("REPORT");
        From = getIntent().getStringExtra("From");

        if (L.isNetworkAvailable(PreReportDetailActivity.this)) {

            if (From.equalsIgnoreCase("Dealer")) {
                getPreReport("dealer_report");
            } else {
                getPreReport("distributor_report");
            }
        }
        swipeContainer.setOnRefreshListener(() -> {
            if (From.equalsIgnoreCase("Dealer")) {
                getPreReport("dealer_report");
            } else {
                getPreReport("distributor_report");
            }
            swipeContainer.setRefreshing(false);
        });

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        preReportAdpater = new PreReportAdpater(this, reportPreListModels);
        recyclerlist.setAdapter(preReportAdpater);
    }

    @OnClick({R.id.img_back, R.id.btn_cutomize})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_cutomize:
                if (From.equalsIgnoreCase("Dealer")) {
                    startActivity(new Intent(this, DealerReportActivity.class));

                } else {
                    startActivity(new Intent(this, DistributorReportActivity.class));
                }
                break;
        }
    }

    private void getPreReport(String dealer_report) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.USERID, user.getId());

        showProgress(true);

        subscription = NetworkRequest.performAsyncRequest(restApi.getDealerReportList(dealer_report, map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    reportPreListModels.clear();
                    reportPreListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("report").toString(), ReportPreListModel.class));
                    preReportAdpater.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


}
