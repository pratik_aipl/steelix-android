package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.EventPicListAdapter;
import com.steelix.beta.model.EventListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class EventActivity extends BaseActivity {

    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    EventPicListAdapter eventPicListAdapter;
    List<EventListModel> eventListModels = new ArrayList<>();

    //  private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    Subscription subscription;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        ButterKnife.bind(this);

        tvTittle.setText("Company Events");
        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        if (L.isNetworkAvailable(this))
            getEventList();

        swipeContainer.setOnRefreshListener(() -> {
            EventActivity.this.getEventList();
            swipeContainer.setRefreshing(false);
        });

        eventPicListAdapter = new EventPicListAdapter(this, eventListModels);
        recyclerlist.setAdapter(eventPicListAdapter);
    }

    private void getEventList() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getEventList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    eventListModels.clear();
                    eventListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.EVENTLIST).toString(), EventListModel.class));
                    eventPicListAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
