package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.DealerDashboardAdpater;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class DistributorListActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    DealerDashboardAdpater dashboardadpater;
    List<DisributorListModel> disributorListModels = new ArrayList<>();

    Subscription subscription;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_list);
        ButterKnife.bind(this);
        tvTittle.setText("My Distributor");

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        if (L.isNetworkAvailable(this)) {
            getDistList();
        }

        swipeContainer.setOnRefreshListener(() -> {
            getDistList();
            swipeContainer.setRefreshing(false);
        });

//        disributorListModels.add(new DisributorListModel(1, "ABC", "7894563210", "a@a.com"));
//        disributorListModels.add(new DisributorListModel(2, "XYZ", "7894563210", "a@a.com"));
//        disributorListModels.add(new DisributorListModel(3, "PQR", "7894563210", "a@a.com"));

        dashboardadpater = new DealerDashboardAdpater(this, disributorListModels);
        recyclerlist.setAdapter(dashboardadpater);
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    private void getDistList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ID, user.getId());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getdistList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    disributorListModels.clear();
                    disributorListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DISTRIBUTORLIST).toString(), DisributorListModel.class));
                    dashboardadpater.notifyDataSetChanged();

                    if (disributorListModels.size() == 0) {
                        recyclerlist.setVisibility(View.GONE);
                        //tvNoDist.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
