package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.steelix.beta.BaseActivity;
import com.steelix.beta.BuildConfig;
import com.steelix.beta.R;
import com.steelix.beta.utils.Constant;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = BuildConfig.DEBUG ? 500 : 3500;
    @BindView(R.id.tv_date)
    TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        tvDate.setText("Copyright@"+Calendar.getInstance().get(Calendar.YEAR));

        new Handler().postDelayed(() -> {
            if (prefs.getBoolean(Constant.isLogin, false)) {

                Intent i = new Intent(this, Dashboard.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
            }
            finish();
        }, SPLASH_TIME_OUT);
    }
}
