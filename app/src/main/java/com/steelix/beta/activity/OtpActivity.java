package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.BuildConfig;
import com.steelix.beta.R;
import com.steelix.beta.model.UserData;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;
import com.steelix.beta.utils.OtpView;
import com.steelix.beta.utils.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class OtpActivity extends BaseActivity {
    private static final String TAG = "OtpActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.btn_resend)
    Button btnResend;
    String otp, mobileno, isfrom, logtype;

    String inputotp;
    Subscription subscription;
    @BindView(R.id.otp_view)
    OtpView otpView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

       /* if (BuildConfig.DEBUG)
            otpView1.setText("123456");*/

        //logtype = getIntent().getStringExtra(Constant.logtype);
        otp = getIntent().getStringExtra(Constant.Otp);
        mobileno = getIntent().getStringExtra(Constant.MOBILENO);
        isfrom = getIntent().getStringExtra(Constant.IsFrom);

        if (BuildConfig.DEBUG) {
            otpView.setOTP("951357");
        }

    }

    @OnClick({R.id.img_back, R.id.btn_confirm, R.id.btn_resend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_confirm:
                inputotp = otpView.getOTP();
                if (Validation.isEmpty(inputotp)) {
                    Toast.makeText(this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                    //   otpView.setError("Please enter OTP");
                } else {
                    if (L.isNetworkAvailable(OtpActivity.this))
                        if (isfrom.equalsIgnoreCase("Login")) {
                            callverify();
                        } else {
                            if (!inputotp.equalsIgnoreCase(otp)) {
                                Toast.makeText(this, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                            } else {
                                Register();
                            }
                        }
                }
                break;
            case R.id.btn_resend:
                if (L.isNetworkAvailable(OtpActivity.this)) {
                    callresendotp();
                }
                break;
        }
    }

    private void callverify() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, mobileno);
        map.put(Constant.Otp, inputotp);
        map.put(Constant.DeviceID, L.getDeviceId(this));


        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getverification(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken, DATA.getString("access_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, Dashboard.class);
                    // intent.putExtra(Constant.logtype, logtype);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void Register() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.FNAME, getIntent().getStringExtra(Constant.FNAME));
        map.put(Constant.LName, getIntent().getStringExtra(Constant.LName));
        map.put(Constant.MOBILENO, getIntent().getStringExtra(Constant.MOBILE));
        map.put(Constant.EMAIL, getIntent().getStringExtra(Constant.EMAIL));
        map.put(Constant.COMPANYNAME, getIntent().getStringExtra(Constant.COMPANYNAME));
        map.put(Constant.STATE, String.valueOf(getIntent().getIntExtra(Constant.STATE, 0)));
        map.put(Constant.CITY, String.valueOf(getIntent().getIntExtra(Constant.CITY, 0)));
        map.put(Constant.ADDRESS, getIntent().getStringExtra(Constant.ADDRESS));
        map.put(Constant.ROLEID, getIntent().getStringExtra(Constant.ROLEID));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getRegister(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {

                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                  /*  prefs.save(Constant.loginAuthToken, DATA.getString("access_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);*/

                    Intent intent = new Intent(this, SuccessRegister.class);
                    //  intent.putExtra(Constant.ROLEID, getIntent().getStringExtra(Constant.ROLEID));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void callresendotp() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, mobileno);
        map.put(Constant.DeviceID, L.getDeviceId(this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getresendotp(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}
