package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.steelix.beta.R;
import com.steelix.beta.adapter.ZoomSliderAdapter;
import com.steelix.beta.model.CompanyOfferModel;
import com.steelix.beta.model.ProductsImage;
import com.steelix.beta.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ZoomImagePager extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    ViewPagerIndicator indicator;
    private List<ProductsImage> eventListModels = new ArrayList<>();
    private List<CompanyOfferModel> companyOfferModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_pager);
        ButterKnife.bind(this);

        if (getIntent().getStringExtra("SFrom").equalsIgnoreCase("Event")) {
            eventListModels = (List<ProductsImage>) getIntent().getSerializableExtra(Constant.PAGERIMAGE);
            pager.setAdapter(new ZoomSliderAdapter(this, eventListModels));
        } else {
            companyOfferModels = (List<CompanyOfferModel>) getIntent().getSerializableExtra(Constant.PAGERIMAGE);
            pager.setAdapter(new ZoomSliderAdapter(this, companyOfferModels,true));
        }

        indicator.setupWithViewPager(pager);
    }
}
