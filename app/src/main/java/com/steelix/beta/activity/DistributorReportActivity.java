package com.steelix.beta.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.TextView;

import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.TabPagerAdapter;
import com.steelix.beta.fregment.DealerwiseReportFragment;
import com.steelix.beta.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DistributorReportActivity extends BaseActivity {

    @BindView(R.id.tab_Inquiry)
    TabLayout tabInquiry;
    @BindView(R.id.pager_Inq)
    ViewPager pagerInq;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_report);
        ButterKnife.bind(this);

        tvTittle.setText("Reports");
        setupViewPager(pagerInq);
//        pagerInq.setOffscreenPageLimit(2);
        tabInquiry.setupWithViewPager(pagerInq);
    }

    private void setupViewPager(ViewPager viewPager) {
        TabPagerAdapter viewPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(DealerwiseReportFragment.newInstance(true), Constant.DealerWise);
        viewPagerAdapter.addFragment(DealerwiseReportFragment.newInstance(false), Constant.ProductWise);

//        viewPagerAdapter.addFragment(new ProductwiseReportFragment(), Constant.ProductWise);

        viewPager.setAdapter(viewPagerAdapter);
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
