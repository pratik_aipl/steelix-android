package com.steelix.beta.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.DistOrderListAdpater;
import com.steelix.beta.model.DistOrderListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

@SuppressLint("Registered")
public class DistributorOrderListActivity extends BaseActivity {

    private static final String TAG = "OrderListActivity";
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    DistOrderListAdpater distOrderListAdpater;
    List<DistOrderListModel> MaindistOrderListModels = new ArrayList<>();
    Subscription subscription;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        tvTittle.setText("Order List");
        if (L.isNetworkAvailable(this)) {
            getDealerList();
        }
        distOrderListAdpater = new DistOrderListAdpater(this, MaindistOrderListModels);
        recyclerList.setAdapter(distOrderListAdpater);
        swipeContainer.setOnRefreshListener(() -> {
            getDealerList();
            swipeContainer.setRefreshing(false);
        });

    }

    private void getDealerList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DistributorID, user.getId());
        map.put("week", "1");

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDealerList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    MaindistOrderListModels.clear();
                    List<DistOrderListModel> distOrderListModels = LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.DEALERLIST).toString(), DistOrderListModel.class);
                    if (distOrderListModels != null && distOrderListModels.size() > 0) {
                        String s = "", temps;
                        for (int i = 0; i < distOrderListModels.size(); i++) {
                            if (s.equals("")) {
                                s = L.getDate(distOrderListModels.get(i).getOrderDate());
                                distOrderListModels.get(i).setHeader(true);
                            }
                            temps = L.getDate(distOrderListModels.get(i).getOrderDate());
                            if ((!distOrderListModels.get(i).isHeader()) && s.equals(temps)) {
                                distOrderListModels.get(i).setHeader(false);
                            } else {
                                s = L.getDate(distOrderListModels.get(i).getOrderDate());
                                distOrderListModels.get(i).setHeader(true);
                            }
                        }
//                            messageList.clear();
                        MaindistOrderListModels.addAll(distOrderListModels);
                        distOrderListAdpater.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
