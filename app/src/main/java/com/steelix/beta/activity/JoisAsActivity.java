package com.steelix.beta.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.steelix.beta.R;
import com.steelix.beta.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JoisAsActivity extends AppCompatActivity {

    @BindView(R.id.tv_dealer)
    Button tvDealer;
    @BindView(R.id.tv_disttri)
    Button tvDisttri;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jois_as);
        ButterKnife.bind(this);
        imgBack.setVisibility(View.GONE);
        tvTittle.setText("Join As");
    }

    @OnClick({R.id.tv_dealer, R.id.tv_disttri})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_dealer:
                startActivity(new Intent(JoisAsActivity.this, RegisterActivity.class).putExtra(Constant.logtype, "3"));
                break;
            case R.id.tv_disttri:
                startActivity(new Intent(JoisAsActivity.this, RegisterActivity.class).putExtra(Constant.logtype, "2"));
                break;
        }
    }
}
