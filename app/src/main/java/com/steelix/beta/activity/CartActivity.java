package com.steelix.beta.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.steelix.beta.BaseActivity;
import com.steelix.beta.R;
import com.steelix.beta.adapter.CartProductsAdpater;
import com.steelix.beta.listner.CartCountEvent;
import com.steelix.beta.listner.RemoveProduct;
import com.steelix.beta.listner.RemoveProductEvent;
import com.steelix.beta.model.DisributorListModel;
import com.steelix.beta.model.ProductsListModel;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class CartActivity extends BaseActivity implements RemoveProduct {
    private static final String TAG = "CartActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_orderDate)
    TextView tvOrderDate;

    final Calendar myCalendar = Calendar.getInstance();
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    CartProductsAdpater cartProductsAdpater;
    DisttListAdapter disttListAdapter;

    List<ProductsListModel> productsListModels = new ArrayList<>();
    List<DisributorListModel> disributorListModels = new ArrayList<>();
    @BindView(R.id.spn_distlist)
    Spinner spnDistlist;
    @BindView(R.id.chk_condition)
    CheckBox chkCondition;
    String DistId;

    Subscription subscription;
    String SelectedDATE = null;
    DatePickerDialog datePickerDialog;
    @BindView(R.id.tv_cartempty)
    TextView tvCartempty;
    @BindView(R.id.li_cartmain)
    LinearLayout liCartmain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        tvTittle.setText("Cart");
        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            productsListModels.addAll(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
        }

        disributorListModels = (List<DisributorListModel>) getIntent().getSerializableExtra(Constant.DISTRIBUTORLIST);
        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());

        if (!productsListModels.isEmpty()) {
            cartProductsAdpater = new CartProductsAdpater(this, productsListModels);
            recyclerlist.setAdapter(cartProductsAdpater);
        } else {
            liCartmain.setVisibility(View.GONE);
            tvCartempty.setVisibility(View.VISIBLE);
        }

        disttListAdapter = new DisttListAdapter(this, disributorListModels);
        spnDistlist.setAdapter(disttListAdapter);

        spnDistlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    DistId = String.valueOf(disributorListModels.get(position-1).getDistributorID());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        tvOrderDate.setText(formattedDate);

        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SelectedDATE = sdf.format(myCalendar.getTime());

    }

    @OnClick({R.id.img_back, R.id.btn_placeOrder, R.id.tv_orderDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_placeOrder:

                if (disributorListModels.size() == 0 || spnDistlist.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please Select Distributor", Toast.LENGTH_SHORT).show();
                } else if (SelectedDATE == null) {
                    Toast.makeText(this, "Please Select Delivery Date", Toast.LENGTH_SHORT).show();
                } else if (!chkCondition.isChecked()) {
                    Toast.makeText(this, "Please Checked Order", Toast.LENGTH_SHORT).show();
                } else {
                    if (L.isNetworkAvailable(CartActivity.this)) {
                        placeOrder();
                    }
                }
                break;

            case R.id.tv_orderDate:
                datePickerDialog = new DatePickerDialog(CartActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }
    }


    private void placeOrder() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ORDERDATE, SelectedDATE);
        map.put(Constant.DISTRIBUTORID, DistId);
        map.put(Constant.DEALERID, user.getId());
        map.put(Constant.ProductID, gson.toJson(productsListModels));
        Log.d(TAG, "placeOrder: "+gson.toJson(productsListModels));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.placeOrder(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    JSONObject orderdetail = DATA.getJSONObject("order");
                    prefs.save(Constant.cartData, "");
                    Intent intent;
                    intent = new Intent(CartActivity.this, OrderPlacedActivity.class);
                    intent.putExtra(Constant.ORDERNO, String.valueOf(orderdetail.get("orderno")));
                    intent.putExtra(Constant.DISTRIBUTORNAME, String.valueOf(orderdetail.get("DistributorName")));
                    intent.putExtra(Constant.DEALERNAME, String.valueOf(orderdetail.get("DealerName")));
                    intent.putExtra(Constant.PQTY, String.valueOf(orderdetail.get("cartQty")));
                    intent.putExtra(Constant.REMAINDAYS, String.valueOf(orderdetail.get("duration")));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvOrderDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        SelectedDATE = sdf.format(myCalendar.getTime());
    }

    DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabel();
    };

    @Override
    public void onRemoveProduct(ProductsListModel productsListModel, int pos) {

        productsListModels.remove(pos);
        cartProductsAdpater.notifyDataSetChanged();
        prefs.save(Constant.cartData, "");
        prefs.save(Constant.cartData, gson.toJson(productsListModels));
        EventBus.getDefault().post(new RemoveProductEvent(pos, productsListModel));
        EventBus.getDefault().post(new CartCountEvent(productsListModels.size()));
        if (productsListModels.isEmpty()) {
            liCartmain.setVisibility(View.GONE);
            tvCartempty.setVisibility(View.VISIBLE);
        }
    }

    class DisttListAdapter extends BaseAdapter {
        List<DisributorListModel> disributorListModels;
        LayoutInflater inflter;
        Context context;

        public DisttListAdapter(Context context, List<DisributorListModel> disributorListModels) {
            this.context = context;
            this.disributorListModels = disributorListModels;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return disributorListModels.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select Distributor");
                names.setTextColor(ContextCompat.getColor(context, R.color.lightblue));
            } else {
                DisributorListModel disributorListModel = disributorListModels.get(position - 1);
                names.setText(disributorListModel.getCompanyName());
                //  names.setTextColor(Color.BLACK);
            }
            return convertView;
        }

    }
}
