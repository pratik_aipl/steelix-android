package com.steelix.beta.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.steelix.beta.BaseActivity;
import com.steelix.beta.BuildConfig;
import com.steelix.beta.R;
import com.steelix.beta.network.NetworkRequest;
import com.steelix.beta.utils.Constant;
import com.steelix.beta.utils.L;
import com.steelix.beta.utils.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;
    @BindView(R.id.btn_lognxt)
    Button btnLognxt;
    @BindView(R.id.tv_clickhere)
    TextView tvClickhere;
    @BindView(R.id.dealer)
    RadioButton dealer;
    @BindView(R.id.distr)
    RadioButton distr;
    @BindView(R.id.rgFilterType)
    RadioGroup rgFilterType;

    Subscription subscription;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    // String logtype = "3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        tvClickhere.setPaintFlags(tvClickhere.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvSkip.setPaintFlags(tvSkip.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (BuildConfig.DEBUG) {
            edtMobileno.setText("12345678");
            edtMobileno.setText("7600566224");
            edtMobileno.setText("8347665465");
        }

    /*    rgFilterType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.dealer:
                        logtype = "3";
                        break;
                    case R.id.distr:
                        logtype = "2";
                        break;

                }
            }
        });*/

    }

    @OnClick({R.id.btn_lognxt, R.id.tv_clickhere, R.id.tv_skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_lognxt:
                if (Validation.isEmpty(L.getEditText(edtMobileno))) {
                    edtMobileno.setError("Please enter Mobile No");
                } else {
                    if (L.isNetworkAvailable(LoginActivity.this)) {
                        callLogin();
                        //startActivity(new Intent(LoginActivity.this, OtpActivity.class));
                    }
                }
                break;
            case R.id.tv_clickhere:
                startActivity(new Intent(LoginActivity.this, JoisAsActivity.class));
                break;
            case R.id.tv_skip:
                startActivity(new Intent(LoginActivity.this, Dashboard.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
        }
    }


    public void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, L.getEditText(edtMobileno));
        map.put(Constant.DeviceID, L.getDeviceId(LoginActivity.this));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject(Constant.data);

                    Intent intent = new Intent(LoginActivity.this, OtpActivity.class);

                    intent.putExtra(Constant.Otp, phone.getString("otp"));
                    intent.putExtra(Constant.MOBILENO, L.getEditText(edtMobileno));
                    intent.putExtra(Constant.IsFrom, "Login");
                    // intent.putExtra(Constant.logtype, logtype);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}
